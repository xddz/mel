#pragma once

typedef void u0;

// Unsigned int types.
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

// Signed int types.
typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long long s64;

// Floating point types
typedef float f32;
typedef double f64;

// Boolean types
typedef unsigned char b8;
typedef unsigned short b16;
typedef unsigned int b32;
typedef unsigned long long b64;

// Properly define static assertions.
#if defined(__clang__) || defined(__gcc__)
#define STATIC_ASSERT _Static_assert
#else
#define STATIC_ASSERT static_assert
#endif

// Ensure all types are of the correct size.
STATIC_ASSERT(sizeof(u8) == 1, "Expected u8 to be 1 byte.");
STATIC_ASSERT(sizeof(u16) == 2, "Expected u16 to be 2 bytes.");
STATIC_ASSERT(sizeof(u32) == 4, "Expected u32 to be 4 bytes.");
STATIC_ASSERT(sizeof(u64) == 8, "Expected u64 to be 8 bytes.");

STATIC_ASSERT(sizeof(s8) == 1, "Expected s8 to be 1 byte.");
STATIC_ASSERT(sizeof(s16) == 2, "Expected s16 to be 2 bytes.");
STATIC_ASSERT(sizeof(s32) == 4, "Expected s32 to be 4 bytes.");
STATIC_ASSERT(sizeof(s64) == 8, "Expected s64 to be 8 bytes.");

STATIC_ASSERT(sizeof(f32) == 4, "Expected f32 to be 4 bytes.");
STATIC_ASSERT(sizeof(f64) == 8, "Expected f64 to be 8 bytes.");

STATIC_ASSERT(sizeof(b8) == 1, "Expected u8 to be 1 byte.");
STATIC_ASSERT(sizeof(b16) == 2, "Expected u16 to be 2 bytes.");
STATIC_ASSERT(sizeof(b32) == 4, "Expected u32 to be 4 bytes.");
STATIC_ASSERT(sizeof(b64) == 8, "Expected u64 to be 8 bytes.");

#define true (1)
#define false (0)

// Platform detection
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) 
#define M_PLATFORM_WINDOWS
#ifndef _WIN64
// TODO: allow for 32-bit
#error "64-bit is required on Windows!"
#endif
#elif defined(__linux__) || defined(__gnu_linux__)
// Linux OS
#define M_PLATFORM_LINUX
#if defined(__ANDROID__)
#define M_PLATFORM_ANDROID
#endif
#elif defined(__unix__)
// Catch anything not caught by the above.
#define M_PLATFORM_UNIX
#elif defined(_POSIX_VERSION)
// Posix
#define M_PLATFORM_POSIX
#elif __APPLE__
// Apple platforms
#define M_PLATFORM_APPLE
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR
// iOS Simulator
#define M_PLATFORM_IOS
#define M_PLATFORM_IOS_SIMULATOR
#elif TARGET_OS_IPHONE
#define M_PLATFORM_IOS
// iOS device
#elif TARGET_OS_MAC
// Other kinds of Mac OS
#else
#error "Unknown Apple platform"
#endif
#else
#error "Unknown platform!"
#endif

#ifdef M_EXPORT
// Exports
#ifdef _MSC_VER
#define M_API __declspec(dllexport)
#else
#define M_API __attribute__((visibility("default")))
#endif
#else
// Imports
#ifdef _MSC_VER
#define M_API __declspec(dllimport)
#else
#define M_API
#endif
#endif

// Inlining
#ifdef _MSC_VER
#define M_INLINE __forceinline
#define M_NOINLINE __declspec(noinline)
#else
#define M_INLINE static inline
#define M_NOINLINE
#endif

/* convenience */
#define str(x) #x
#define M_CLAMP(value, min, max) ((value <= min) ? min : ((value >= max) ? max : value))
