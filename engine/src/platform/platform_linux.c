#include "./platform.h"

#ifdef M_PLATFORM_LINUX

#include <core/logger.h>
#include <core/event.h>
#include <core/input.h>

#include <containers/darray.h>

#include <xcb/xcb.h>
#include <xcb/xkb.h> /* sudo apt-get install libxkbcommon-x11-dev */
#include <X11/keysym.h>
#include <X11/XKBlib.h> /* sudo apt-get install libx11-dev */
#include <X11/Xlib.h>
#include <X11/Xlib-xcb.h> /* sudo apt-get install libxkbcommon-x11-dev */
#include <sys/time.h>

#if _POSIX_C_SOURCE >= 199309L
#include <time.h> /* nanosleep() */
#else
#include <unistd.h> /* usleep() */
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* surface creation headers  */
#define VK_USE_PLATFORM_XCB_KHR
#include <vulkan/vulkan.h>
#include "renderer/vulkan/vulkan_types.h"
/* * * * * * * * * * * * * * */

typedef struct internal_state {
  Display * display;
  xcb_connection_t * connection;
  xcb_window_t window;
  xcb_screen_t * screen;
  xcb_atom_t wm_protocols;
  xcb_atom_t wm_delete_win;
  VkSurfaceKHR surface;
} internal_state;

//case XK_Shift: return KEY_SHIFT;
//case XK_Control: return KEY_CONTROL;
// Not supported
// case : return KEY_CONVERT;
// case : return KEY_NONCONVERT;
// case : return KEY_ACCEPT;
// case XK_snapshot: return KEY_SNAPSHOT; // not supported
// case XK_apps: return KEY_APPS; // not supported
// case XK_sleep: return KEY_SLEEP; //not supported
// case XK_Menu: return KEY_LMENU;

/* key codes */
static keys keycodes[] = {
  [XK_BackSpace] = KEY_BACKSPACE,
  [XK_Return] = KEY_ENTER,
  [XK_Tab] = KEY_TAB,
  [XK_Pause] = KEY_PAUSE,
  [XK_Caps_Lock] = KEY_CAPITAL,
  [XK_Escape] = KEY_ESCAPE,
  [XK_Mode_switch] = KEY_MODECHANGE,
  [XK_space] = KEY_SPACE,
  [XK_Prior] = KEY_PRIOR,
  [XK_Next] = KEY_NEXT,
  [XK_End] = KEY_END,
  [XK_Home] = KEY_HOME,
  [XK_Left] = KEY_LEFT,
  [XK_Up] = KEY_UP,
  [XK_Right] = KEY_RIGHT,
  [XK_Down] = KEY_DOWN,
  [XK_Select] = KEY_SELECT,
  [XK_Print] = KEY_PRINT,
  [XK_Execute] = KEY_EXECUTE,
  [XK_Insert] = KEY_INSERT,
  [XK_Delete] = KEY_DELETE,
  [XK_Help] = KEY_HELP,
  [XK_Meta_L] = KEY_LWIN,
  [XK_Meta_R] = KEY_RWIN,
  [XK_KP_0] = KEY_NUMPAD0,
  [XK_KP_1] = KEY_NUMPAD1,
  [XK_KP_2] = KEY_NUMPAD2,
  [XK_KP_3] = KEY_NUMPAD3,
  [XK_KP_4] = KEY_NUMPAD4,
  [XK_KP_5] = KEY_NUMPAD5,
  [XK_KP_6] = KEY_NUMPAD6,
  [XK_KP_7] = KEY_NUMPAD7,
  [XK_KP_8] = KEY_NUMPAD8,
  [XK_KP_9] = KEY_NUMPAD9,
  [XK_multiply] = KEY_MULTIPLY,
  [XK_KP_Add] = KEY_ADD,
  [XK_KP_Separator] = KEY_SEPARATOR,
  [XK_KP_Subtract] = KEY_SUBTRACT,
  [XK_KP_Decimal] = KEY_DECIMAL,
  [XK_KP_Divide] = KEY_DIVIDE,
  [XK_F1] = KEY_F1,
  [XK_F2] = KEY_F2,
  [XK_F3] = KEY_F3,
  [XK_F4] = KEY_F4,
  [XK_F5] = KEY_F5,
  [XK_F6] = KEY_F6,
  [XK_F7] = KEY_F7,
  [XK_F8] = KEY_F8,
  [XK_F9] = KEY_F9,
  [XK_F10] = KEY_F10,
  [XK_F11] = KEY_F11,
  [XK_F12] = KEY_F12,
  [XK_F13] = KEY_F13,
  [XK_F14] = KEY_F14,
  [XK_F15] = KEY_F15,
  [XK_F16] = KEY_F16,
  [XK_F17] = KEY_F17,
  [XK_F18] = KEY_F18,
  [XK_F19] = KEY_F19,
  [XK_F20] = KEY_F20,
  [XK_F21] = KEY_F21,
  [XK_F22] = KEY_F22,
  [XK_F23] = KEY_F23,
  [XK_F24] = KEY_F24,
  [XK_Num_Lock] = KEY_NUMLOCK,
  [XK_Scroll_Lock] = KEY_SCROLL,
  [XK_KP_Equal] = KEY_NUMPAD_EQUAL,
  [XK_Shift_L] = KEY_LSHIFT,
  [XK_Shift_R] = KEY_RSHIFT,
  [XK_Control_L] = KEY_LCONTROL,
  [XK_Control_R] = KEY_RCONTROL,
  [XK_Menu] = KEY_RMENU,
  [XK_semicolon] = KEY_SEMICOLON,
  [XK_plus] = KEY_PLUS,
  [XK_comma] = KEY_COMMA,
  [XK_minus] = KEY_MINUS,
  [XK_period] = KEY_PERIOD,
  [XK_slash] = KEY_SLASH,
  [XK_grave] = KEY_GRAVE,
  [XK_a] = KEY_A,
  [XK_A] = KEY_A,
  [XK_b] = KEY_B,
  [XK_B] = KEY_B,
  [XK_c] = KEY_C,
  [XK_C] = KEY_C,
  [XK_d] = KEY_D,
  [XK_D] = KEY_D,
  [XK_e] = KEY_E,
  [XK_E] = KEY_E,
  [XK_f] = KEY_F,
  [XK_F] = KEY_F,
  [XK_g] = KEY_G,
  [XK_G] = KEY_G,
  [XK_h] = KEY_H,
  [XK_H] = KEY_H,
  [XK_i] = KEY_I,
  [XK_I] = KEY_I,
  [XK_j] = KEY_J,
  [XK_J] = KEY_J,
  [XK_k] = KEY_K,
  [XK_K] = KEY_K,
  [XK_l] = KEY_L,
  [XK_L] = KEY_L,
  [XK_m] = KEY_M,
  [XK_M] = KEY_M,
  [XK_n] = KEY_N,
  [XK_N] = KEY_N,
  [XK_o] = KEY_O,
  [XK_O] = KEY_O,
  [XK_p] = KEY_P,
  [XK_P] = KEY_P,
  [XK_q] = KEY_Q,
  [XK_Q] = KEY_Q,
  [XK_r] = KEY_R,
  [XK_R] = KEY_R,
  [XK_s] = KEY_S,
  [XK_S] = KEY_S,
  [XK_t] = KEY_T,
  [XK_T] = KEY_T,
  [XK_u] = KEY_U,
  [XK_U] = KEY_U,
  [XK_v] = KEY_V,
  [XK_V] = KEY_V,
  [XK_w] = KEY_W,
  [XK_W] = KEY_W,
  [XK_x] = KEY_X,
  [XK_X] = KEY_X,
  [XK_y] = KEY_Y,
  [XK_Y] = KEY_Y,
  [XK_z] = KEY_Z,
  [XK_Z] = KEY_Z,
};
/* * * * * * */

b8 platform_startup(platform_state * plat_state, 
                    const char * const name, 
                    s16 x,     s16 y, 
                    u16 width, u16 height) {

  /* create internal state */
  plat_state->internal_state = malloc(sizeof(internal_state));
  internal_state * state = (internal_state*)plat_state->internal_state;
  /* * * * * * * * * * * * */

  /* connect to X server */
  state->display = XOpenDisplay(NULL);
  /* * * * * * * * * * * */

  /* retrieve connection from display  */
  state->connection = XGetXCBConnection(state->display);

  if (xcb_connection_has_error(state->connection)) {
    M_FATAL("Failed to connect to X server via XCB.");
    return false;
  }
  /* * * * * * * * * * * * * * * * * * */

  /* turn key repeats off  */
  //XAutoRepeatOff(state->display);
  xcb_xkb_use_extension(state->connection, XCB_XKB_MAJOR_VERSION, XCB_XKB_MINOR_VERSION);
  xcb_xkb_per_client_flags(state->connection, XCB_XKB_ID_USE_CORE_KBD, 
                           XCB_XKB_PER_CLIENT_FLAG_DETECTABLE_AUTO_REPEAT, 1, 0, 0, 0);
  /* * * * * * * * * * * * */

  /* loop through available displays */
  //const struct xcb_setup_t * setup = xcb_get_setup(state->connection);
  //xcb_screen_iterator_t it = xcb_setup_roots_iterator(setup);
  //s32 screen_p = 0;

  //for (s32 s = screen_p; s > 0; --s) {
  //  xcb_screen_next(&it);
  //}
  /* * * * * * * * * * * * * * * * * */

  /* assign display  */
  //state->screen = it.data;
  state->screen = xcb_setup_roots_iterator(xcb_get_setup(state->connection)).data;
  /* * * * * * * * * */

  /* generate window XID */
  state->window = xcb_generate_id(state->connection);
  /* * * * * * * * * * * */

  /* register window event types */
  /* 
    * XCB_CW_BACK_PIXEL: fill the window background with a single color 
    * XCB_CW_EVENT_MASK: required flag
  */
  u32 event_mask   = XCB_CW_BACK_PIXEL           | XCB_CW_EVENT_MASK;
  u32 event_values = XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE |
                     XCB_EVENT_MASK_KEY_PRESS    | XCB_EVENT_MASK_KEY_RELEASE    |
                     XCB_EVENT_MASK_EXPOSURE     | XCB_EVENT_MASK_POINTER_MOTION |
                     XCB_EVENT_MASK_STRUCTURE_NOTIFY;
  u32 value_list[] = {state->screen->black_pixel, event_values};
  /* * * * * * * * * * * * * * * */

  /* create window */
  xcb_void_cookie_t cookie = 
                      xcb_create_window(state->connection         , XCB_COPY_FROM_PARENT         ,
                                        state->window             , state->screen->root          ,
                                        x                         , y                            ,
                                        width                     , height                       ,
                                        0                         , XCB_WINDOW_CLASS_INPUT_OUTPUT,
                                        state->screen->root_visual, event_mask                   ,
                                        value_list);
  /* * * * * * * * */

  /* set window title  */
  xcb_change_property(state->connection, XCB_PROP_MODE_REPLACE, 
                      state->window    , XCB_ATOM_WM_NAME     , 
                      XCB_ATOM_STRING  , 8                    , 
                      strlen(name)     , name);
  /* * * * * * * * * * */

  /* make sure the server notifies when 
     the window manager attempts to destroy the window */
  const char * const          delete_string = "WM_DELETE_WINDOW";
  xcb_intern_atom_cookie_t wm_delete_cookie = 
                              xcb_intern_atom(state->connection, 0, 
                                       strlen(delete_string)   , delete_string);

  const char * const          protocols_string = "WM_PROTOCOLS";
  xcb_intern_atom_cookie_t wm_protocols_cookie = 
                              xcb_intern_atom(state->connection, 0, 
                                       strlen(protocols_string), protocols_string);

  xcb_intern_atom_reply_t * wm_delete_reply = 
                              xcb_intern_atom_reply(state->connection, wm_delete_cookie, NULL);

  xcb_intern_atom_reply_t * wm_protocols_reply = 
                              xcb_intern_atom_reply(state->connection, wm_protocols_cookie, NULL);

  state->wm_delete_win = wm_delete_reply->atom;
  state->wm_protocols  = wm_protocols_reply->atom;
  /* * * * * * * * * * * * * * * * * * * * * * * * * * */

  /* bind window callbacks */
  xcb_change_property(state->connection, XCB_PROP_MODE_REPLACE   , 
                      state->window    , wm_protocols_reply->atom,
                      4                , 32                      , 
                      1                , &wm_delete_reply->atom);
  /* * * * * * * * * * * * */

  /* map window to screen  */
  xcb_map_window(state->connection, state->window);
  /* * * * * * * * * * * * */

  /* flush stream  */
  s32 stream_result = xcb_flush(state->connection);
  if (stream_result <= 0) {
    M_FATAL("An error occurred when flushing the stream: %d", stream_result);
    return false;
  }
  /* * * * * * * * */

  return true;
}

u0 platform_shutdown(platform_state * plat_state) {
  internal_state * state = (internal_state*)plat_state->internal_state;

  /* turn key repeats back on  */
  //XAutoRepeatOn(state->display);
  /* * * * * * * * * * * * * * */

  xcb_destroy_window(state->connection, state->window);
}

b8 platform_pump_messages(platform_state * plat_state) {
  internal_state * state = (internal_state*)plat_state->internal_state;

  xcb_generic_event_t * event;
  xcb_client_message_event_t * cm;

  b8 should_quit = false;
  
  /* poll events */
  while (event != 0) {
    if (0 == (event = xcb_poll_for_event(state->connection))) {
      break;
    }

    /* handle input events regardless of input origin  */
    switch (event->response_type & ~0x80) {
      default:
        break;

      case XCB_KEY_PRESS:
      case XCB_KEY_RELEASE: {
        xcb_key_press_event_t * kb_event = (xcb_key_press_event_t *)event;
        b8 pressed = event->response_type == XCB_KEY_PRESS;
        xcb_keycode_t code = kb_event->detail;
        KeySym key_sym = XkbKeycodeToKeysym(state->display, (KeyCode)code, 0, code & ShiftMask ? 1 : 0);
        
        input_process_key(keycodes[key_sym], pressed);
        break;
      }

      case XCB_BUTTON_PRESS:
      case XCB_BUTTON_RELEASE: {
        xcb_button_press_event_t * mouse_event = (xcb_button_press_event_t *)event;
        b8 pressed = event->response_type == XCB_BUTTON_PRESS;
        buttons mouse_button = BUTTON_MAX_BUTTONS;
        switch (mouse_event->detail) {
          case XCB_BUTTON_INDEX_1:
            mouse_button = BUTTON_LEFT;
            break;
          case XCB_BUTTON_INDEX_2:
            mouse_button = BUTTON_MIDDLE;
            break;
          case XCB_BUTTON_INDEX_3:
            mouse_button = BUTTON_RIGHT;
            break;
        }
        if (mouse_button != BUTTON_MAX_BUTTONS) {
          input_process_button(mouse_button, pressed);
        }
        break;
      }

      case XCB_MOTION_NOTIFY: {
        /* mouse coordinates have changed */
        xcb_motion_notify_event_t * move_event = (xcb_motion_notify_event_t *)event;
        input_process_mouse_move(move_event->event_x, move_event->event_y);
        break;
      }

      case XCB_CONFIGURE_NOTIFY: {
        /* window has been resized */
        /* possibly triggered by moving
         * the window since a change in x,y coordinates
         * may also mean a resize in the top left corner */
        xcb_configure_notify_event_t * configure_event = (xcb_configure_notify_event_t *)event;
        event_context context;
        context.data.u16[0] = configure_event->width;
        context.data.u16[1] = configure_event->height;
        event_fire(EVENT_CODE_RESIZED, 0, context);
        break;
      }

      case XCB_CLIENT_MESSAGE: {
        /* TODO: event -> quit() */
        cm = (xcb_client_message_event_t *)event;

        if (cm->data.data32[0] == state->wm_delete_win) {
          should_quit = true;
        }
        break;
      }
    }
    /* * * * * * * * * * * * * * * * * * * * * * * * * */

    free(event);
  }
  /* * * * * * * */

  return !should_quit;
}

u0 * platform_allocate(u64 size,
                         b8 aligned) {

  return malloc(size);
}

u0 platform_free(u0 * block,
                   b8 aligned) {

  free(block);
}

u0 * platform_zero_memory(u0 * block,
                            u64 size) {

  return memset(block, 0, size);
}

u0 * platform_copy_memory(u0 * dest,
                            const u0 * source,
                            u64 size) {

  return memcpy(dest, source, size);
}

u0 * platform_set_memory(u0 * dest,
                           s32 value,
                           u64 size) {

  return memset(dest, value, size);
}

u0 platform_console_write(const char * const message,
                            u8 color) {

  /* fatal, error, warn, info, debug, trace */
  const char * const colors[] = {"0;41", "1;31", "1;33", "1;32", "1;34", "1;30"};
  printf("\033[%sm%s\033[0m\n", colors[color], message);
}

u0 platform_console_write_error(const char * const message,
                                  u8 color) {

  /* fatal, error, warn, info, debug, trace */
  const char * const colors[] = {"0;41", "1;31", "1;33", "1;32", "1;34", "1;30"};
  printf("\033[%sm%s\033[0m\n", colors[color], message);
}

f64 platform_get_absolute_time(u0) {
  struct timespec now;
#ifdef CLOCK_MONOTONIC
  if (0 != clock_gettime(CLOCK_MONOTONIC, &now))
#endif
    clock_gettime(CLOCK_REALTIME, &now);
  return now.tv_sec + (now.tv_nsec * 0.000000001);
}

/* block the main thread 
 * for the provided ms */
u0 platform_sleep(const u64 ms) {
#if _POSIX_C_SOURCE >= 199309L
  struct timespec ts;
  ts.tv_sec = ms * 0.001;
  ts.tv_nsec = (ms % 1000) * 1000 * 1000;
  nanosleep(&ts, 0);
#else
  if (ms >= 1000) {
    sleep(ms * 0.001);
  }
  usleep((ms % 1000) * 1000);
#endif
}
/* * * * * * * * * * * */

u0 platform_get_required_extension_names(const char *** ext_darray) {
  darray_push(*ext_darray, &"VK_KHR_xcb_surface"); /* optional: use VK_KHR_xlib_surface */
}

b8 platform_create_vulkan_surface(struct platform_state * plat_state,
                                  struct vulkan_context * context) {

  /* create internal state */
  internal_state * state = (internal_state*)plat_state->internal_state;
  /* * * * * * * * * * * * */

  VkXcbSurfaceCreateInfoKHR create_info = {VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR};
  create_info.connection = state->connection;
  create_info.window = state->window;

  VkResult result = vkCreateXcbSurfaceKHR(context->instance, &create_info, context->allocator, &state->surface);
  if (result != VK_SUCCESS) {
    M_FATAL("Failed creating Vulkan surface.");
    return false;
  }
  context->surface = state->surface;
  return true;
}

#endif
