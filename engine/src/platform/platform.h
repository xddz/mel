#pragma once

#include "../defines.h"

typedef struct platform_state {
  u0 * internal_state;
} platform_state;

b8 platform_startup(platform_state * plat_state,
                    const char * const name,
                    s16 x,     s16 y,
                    u16 width, u16 height);

u0 platform_shutdown(platform_state * plat_state);

b8 platform_pump_messages(platform_state * plat_state);

u0 * platform_allocate(u64 size,
                         b8 aligned);

u0 platform_free(u0 * block,
                   b8 aligned);

u0 * platform_zero_memory(u0 * block,
                            u64 size);

u0 * platform_copy_memory(u0 * dest,
                            const u0 * source,
                            u64 size);

u0 * platform_set_memory(u0 * dest,
                           s32 value,
                           u64 size);

u0 platform_console_write(const char * const message,
                            u8 color);

u0 platform_console_write_error(const char * const message,
                                  u8 color);

f64 platform_get_absolute_time(u0);

/* block the main thread 
 * for the provided ms */
u0 platform_sleep(const u64 ms);
/* * * * * * * * * * * */
