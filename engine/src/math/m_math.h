#pragma once

#include <defines.h>
#include <core/m_memory.h>
#include "math_types.h"

/* useful definitions  */
#define M_PI                  ( 3.14159265358979323846f )
#define M_PI_2                ( 2.0f  * M_PI            )
#define M_HALF_PI             ( 0.5f  * M_PI            )
#define M_QUARTER_PI          ( 0.25f * M_PI            )
#define M_ONE_OVER_PI         ( 1.0f  / M_PI            )
#define M_ONE_OVER_TWO_PI     ( 1.0f  / M_PI_2          )
#define M_SQRT_TWO            ( 1.41421356237309504880f )
#define M_SQRT_THREE          ( 1.73205080756887729352f )
#define M_SQRT_ONE_OVER_TWO   ( 0.70710678118654752440f )
#define M_SQRT_ONE_OVER_THREE ( 0.57735026918962576450f )
#define M_DEG2RAD_MULTIPLIER  ( M_PI   / 180.0f         )
#define M_RAD2DEG_MULTIPLIER  ( 180.0f / M_PI           )

#define M_SEC_TO_MS_MULTIPLIER (1000.0f)
#define M_MS_TO_SEC_MULTIPLIER (0.001f)

#define M_INFINITY (1e30f)

#define M_FLOAT_EPSILON (1.192092896e-07f) /* smallest positive n where 1.0 + n != 0 */
/* * * * * * * * * * * */

/* return the sine value of `x`  */
M_API f32 m_sin(const f32 x);
/* * * * * * * * * * * * * * * * */

/* return the cosine value of `x`  */
M_API f32 m_cos(const f32 x);
/* * * * * * * * * * * * * * * * * */

/* return the tangent value of `x` */
M_API f32 m_tan(const f32 x);
/* * * * * * * * * * * * * * * * * */

/* return the arc cosine value of `x`  */
M_API f32 m_acos(const f32 x);
/* * * * * * * * * * * * * * * * * * * */

/* return the square root of `x` */
M_API f32 m_sqrt(const f32 x);
/* * * * * * * * * * * * * * * * */

/* return the absolute value of `x`  */
M_API f32 m_abs(const f32 x);
/* * * * * * * * * * * * * * * * * * */

/* returns whether the
 * value is positive and a power of 2  */
M_INLINE b8 is_power_of_2(const u64 value) {
  return (value != 0) && ((value & (value - 1)) == 0);
}
/* * * * * * * * * * * * * * * * * * * */

/* return an s32 random value */
M_API s32 m_random(u0);
/* * * * * * * * * * * * * * * */

/* return an s32 random value in the provided range  */
M_API s32 m_random_in_range(const s32 min,
                            const s32 max);
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return an f32 random value */
M_API f32 m_frandom(u0);
/* * * * * * * * * * * * * * * */

/* return an f32 random value in the provided range  */
M_API f32 m_frandom_in_range(const f32 min,
                             const f32 max);
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 2 */
M_INLINE vec2 vec2_create(const f32 x,
                          const f32 y) {

  return (vec2){x, y};
}
/* * * * * * * * * * */

/* create a vector 2 initialized to 0  */
M_INLINE vec2 vec2_zero(u0) {
  return (vec2){0.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * */

/* create a vector 2 initialized to 1  */
M_INLINE vec2 vec2_one(u0) {
  return (vec2){1.0f, 1.0f};
}
/* * * * * * * * * * * * * * * * * * * */

/* create a vector 2 representing an up vector */
M_INLINE vec2 vec2_up(u0) {
  return (vec2){0.0f, 1.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 2 representing a downwards vector */
M_INLINE vec2 vec2_down(u0) {
  return (vec2){0.0f, -1.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 2 representing a leftwards vector */
M_INLINE vec2 vec2_left(u0) {
  return (vec2){-1.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 2 representing a right vector */
M_INLINE vec2 vec2_right(u0) {
  return (vec2){1.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * * */

/* return the sum of 2 vector 2s */
M_INLINE vec2 vec2_add(const vec2 vector_0,
                       const vec2 vector_1) {

  return (vec2){
           vector_0.x + vector_1.x,
           vector_0.y + vector_1.y,
         };
}
/* * * * * * * * * * * * * * * * */

/* return the subtraction of 2 vector 2s */
M_INLINE vec2 vec2_sub(const vec2 vector_0,
                       const vec2 vector_1) {

  return (vec2){
           vector_0.x - vector_1.x,
           vector_0.y - vector_1.y,
         };
}
/* * * * * * * * * * * * * * * * * * * * */

/* return the multiplication of 2 vector 2s  */
M_INLINE vec2 vec2_mul(const vec2 vector_0,
                       const vec2 vector_1) {

  return (vec2){
           vector_0.x * vector_1.x,
           vector_0.y * vector_1.y,
         };
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* return the division of 2 vector 2s  */
M_INLINE vec2 vec2_div(const vec2 vector_0,
                       const vec2 vector_1) {

  return (vec2){
           vector_0.x / vector_1.x,
           vector_0.y / vector_1.y,
         };
}
/* * * * * * * * * * * * * * * * * * * */

/* return squared length of a
 * vector 2 using the pithagorean theorem  */
M_INLINE f32 vec2_length_squared(const vec2 vector) {
  return ((vector.x * vector.x) + (vector.y * vector.y));
}
/* * * * * * * * * * * * * * * * * * * * * */

/* return the length of a vector 2 */
M_INLINE f32 vec2_length(const vec2 vector) {
  return m_sqrt(vec2_length_squared(vector));
}
/* * * * * * * * * * * * * * * * * */

/* normalize vector 2 to a unit vector */
M_INLINE u0 vec2_normalize(vec2 * const vector) {
  const f32 length = vec2_length(*vector);
  vector->x /= length;
  vector->y /= length;
}
/* * * * * * * * * * * * * * * * * * * */

/* return a normalized copy of the vector 2  */
M_INLINE vec2 vec2_normalized(vec2 vector) {
  vec2_normalize(&vector);
  return vector;
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* return whether the difference between
 * 2 vector 2s is within the provided tolerance  */
M_INLINE b8 vec2_compare(const vec2 vector_0,
                         const vec2 vector_1,
                         const f32 tolerance) {

  if (m_abs(vector_0.x - vector_1.x) > tolerance) {
    return false;
  }

  if (m_abs(vector_0.y - vector_1.y) > tolerance) {
    return false;
  }

  return true;
}
/* * * * * * * * * * * * * * * * * * * * * * * * */

/* return the distance between 2 vector 2s */
M_INLINE f32 vec2_distance(const vec2 vector_0,
                           const vec2 vector_1) {

  vec2 d = (vec2){
    vector_0.x - vector_1.x,
    vector_0.y - vector_1.y,
  };
  return vec2_length(d);
}
/* * * * * * * * * * * * * * * * * * * * * */

/* create a vector 3 */
M_INLINE vec3 vec3_create(const f32 x,
                          const f32 y,
                          const f32 z) {

  return (vec3){x, y, z};
}
/* * * * * * * * * * */

/* return a vector 3 from a vector 4 */
M_INLINE vec3 vec3_from_vec4(const vec4 vector) {
  return (vec3){vector.x, vector.y, vector.z};
}
/* * * * * * * * * * * * * * * * * * */

/* return vector 4 composed of vector 3  */
M_INLINE vec4 vec3_to_vec4(const vec3 vector,
                           const f32 w) {

  return (vec4){vector.x, vector.y, vector.z, w};
}
/* * * * * * * * * * * * * * * * * * * * */

/* create a vector 3 initialized to 0  */
M_INLINE vec3 vec3_zero(u0) {
  return (vec3){0.0f, 0.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * */

/* create a vector 3 initialized to 1  */
M_INLINE vec3 vec3_one(u0) {
  return (vec3){1.0f, 1.0f, 1.0f};
}
/* * * * * * * * * * * * * * * * * * * */

/* create a vector 3 representing an up vector */
M_INLINE vec3 vec3_up(u0) {
  return (vec3){0.0f, 1.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 3 representing a downwards vector */
M_INLINE vec3 vec3_down(u0) {
  return (vec3){0.0f, -1.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 3 representing a leftwards vector */
M_INLINE vec3 vec3_left(u0) {
  return (vec3){-1.0f, 0.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 3 representing a right vector */
M_INLINE vec3 vec3_right(u0) {
  return (vec3){1.0f, 0.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 3 representing a forward vector */
M_INLINE vec3 vec3_forward(u0) {
  return (vec3){0.0f, 0.0f, -1.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * * * */

/* create a vector 3 representing a backwards vector */
M_INLINE vec3 vec3_back(u0) {
  return (vec3){0.0f, 0.0f, 1.0f};
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return the sum of 2 vector 3s */
M_INLINE vec3 vec3_add(const vec3 vector_0,
                       const vec3 vector_1) {

  return (vec3){
           vector_0.x + vector_1.x,
           vector_0.y + vector_1.y,
           vector_0.z + vector_1.z
         };
}
/* * * * * * * * * * * * * * * * */

/* return the subtraction of 2 vector 3s */
M_INLINE vec3 vec3_sub(const vec3 vector_0,
                       const vec3 vector_1) {

  return (vec3){
           vector_0.x - vector_1.x,
           vector_0.y - vector_1.y,
           vector_0.z - vector_1.z
         };
}
/* * * * * * * * * * * * * * * * * * * * */

/* return the multiplication of 2 vector 3s  */
M_INLINE vec3 vec3_mul(const vec3 vector_0,
                       const vec3 vector_1) {

  return (vec3){
           vector_0.x * vector_1.x,
           vector_0.y * vector_1.y,
           vector_0.z * vector_1.z
         };
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* return the multiplication of a vector 2 by a scalar */
M_INLINE vec3 vec3_mul_scalar(const vec3 vector_0,
                              const f32 scalar) {

  return (vec3){
           vector_0.x * scalar,
           vector_0.y * scalar,
           vector_0.z * scalar
         };
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return the division of 2 vector 3s  */
M_INLINE vec3 vec3_div(const vec3 vector_0,
                       const vec3 vector_1) {

  return (vec3){
           vector_0.x / vector_1.x,
           vector_0.y / vector_1.y,
           vector_0.z / vector_1.z
         };
}
/* * * * * * * * * * * * * * * * * * * */

/* return squared length of a
 * vector 3 using the pithagorean theorem  */
M_INLINE f32 vec3_length_squared(const vec3 vector) {
  return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z;
}
/* * * * * * * * * * * * * * * * * * * * * */

/* return the length of a vector 3 */
M_INLINE f32 vec3_length(const vec3 vector) {
  return m_sqrt(vec3_length_squared(vector));
}
/* * * * * * * * * * * * * * * * * */

/* normalize vector 3 to a unit vector */
M_INLINE u0 vec3_normalize(vec3 * const vector) {
  const f32 length = vec3_length(*vector);
  vector->x /= length;
  vector->y /= length;
  vector->z /= length;
}
/* * * * * * * * * * * * * * * * * * * */

/* return a normalized copy of the vector 3  */
M_INLINE vec3 vec3_normalized(vec3 vector) {
  vec3_normalize(&vector);
  return vector;
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* return the dot product between 2 vector 3s  */
M_INLINE f32 vec3_dot(const vec3 vector_0,
                      const vec3 vector_1) {

  f32 p = 0;
  p += vector_0.x * vector_1.x;
  p += vector_0.y * vector_1.y;
  p += vector_0.z * vector_1.z;
  return p;
}
/* * * * * * * * * * * * * * * * * * * * * * * */

/* return the cross product between 2 vector 3s  */
M_INLINE vec3 vec3_cross(const vec3 vector_0,
                         const vec3 vector_1) {

  return (vec3){
           vector_0.y * vector_1.z - vector_0.z * vector_1.y,
           vector_0.z * vector_1.x - vector_0.x * vector_1.z,
           vector_0.x * vector_1.y - vector_0.y * vector_1.x
         };
}
/* * * * * * * * * * * * * * * * * * * * * * * * */

/* return whether the difference between
 * 2 vector 2s is within the provided tolerance  */
M_INLINE b8 vec3_compare(const vec3 vector_0,
                         const vec3 vector_1,
                         const f32 tolerance) {

  if (m_abs(vector_0.x - vector_1.x) > tolerance) {
    return false;
  }

  if (m_abs(vector_0.y - vector_1.y) > tolerance) {
    return false;
  }

  if (m_abs(vector_0.z - vector_1.z) > tolerance) {
    return false;
  }

  return true;
}
/* * * * * * * * * * * * * * * * * * * * * * * * */

/* return the distance between 2 vector 3s */
M_INLINE f32 vec3_distance(const vec3 vector_0,
                           const vec3 vector_1) {

  vec3 d = (vec3){
    vector_0.x - vector_1.x,
    vector_0.y - vector_1.y,
    vector_0.z - vector_1.z,
  };
  return vec3_length(d);
}
/* * * * * * * * * * * * * * * * * * * * * */

/* create a vector 4 */
M_INLINE vec4 vec4_create(const f32 x,
                          const f32 y,
                          const f32 z,
                          const f32 w) {

  vec4 out_vector;
#if defined(M_USE_SIMD)
  out_vector.data = _mm_setr_ps(x, y, z, w);
#else
  out_vector.x = x;
  out_vector.y = y;
  out_vector.z = z;
  out_vector.w = w;
#endif
  return out_vector;
}
/* * * * * * * * * * */

/* return vector 3 composed of vector 4  */
M_INLINE vec3 vec4_to_vec3(const vec4 vector) {
  return (vec3){vector.x, vector.y, vector.z};
}
/* * * * * * * * * * * * * * * * * * * * */

/* return a vector 4 from a vector 3 */
M_INLINE vec4 vec4_from_vec3(const vec3 vector,
                             const f32 w) {

#if defined(M_USE_SIMD)
  vec4 out_vector;
  out_vector.data = _mm_setr_ps(vector.x,
                                vector.y,
                                vector.z,
                                w);
  return out_vector;
#else
  return (vec4){vector.x, vector.y, vector.z, w};
#endif
}
/* * * * * * * * * * * * * * * * * * */

/* create a vector 4 initialized to 0  */
M_INLINE vec4 vec4_zero(u0) {
  return (vec4){0.0f, 0.0f, 0.0f, 0.0f};
}
/* * * * * * * * * * * * * * * * * * * */

/* create a vector 4 initialized to 1  */
M_INLINE vec4 vec4_one(u0) {
  return (vec4){1.0f, 1.0f, 1.0f, 1.0f};
}
/* * * * * * * * * * * * * * * * * * * */

/* return the sum of 2 vector 4s */
M_INLINE vec4 vec4_add(const vec4 vector_0,
                       const vec4 vector_1) {

  vec4 result;
  result.elements[0] = vector_0.elements[0] + vector_1.elements[0];
  result.elements[1] = vector_0.elements[1] + vector_1.elements[1];
  result.elements[2] = vector_0.elements[2] + vector_1.elements[2];
  result.elements[3] = vector_0.elements[3] + vector_1.elements[3];
  return result;
}
/* * * * * * * * * * * * * * * * */

/* return the subtraction of 2 vector 4s */
M_INLINE vec4 vec4_sub(const vec4 vector_0,
                       const vec4 vector_1) {

  vec4 result;
  result.elements[0] = vector_0.elements[0] - vector_1.elements[0];
  result.elements[1] = vector_0.elements[1] - vector_1.elements[1];
  result.elements[2] = vector_0.elements[2] - vector_1.elements[2];
  result.elements[3] = vector_0.elements[3] - vector_1.elements[3];
  return result;
}
/* * * * * * * * * * * * * * * * * * * * */

/* return the multiplication of 2 vector 4s  */
M_INLINE vec4 vec4_mul(const vec4 vector_0,
                       const vec4 vector_1) {

  vec4 result;
  result.elements[0] = vector_0.elements[0] * vector_1.elements[0];
  result.elements[1] = vector_0.elements[1] * vector_1.elements[1];
  result.elements[2] = vector_0.elements[2] * vector_1.elements[2];
  result.elements[3] = vector_0.elements[3] * vector_1.elements[3];
  return result;
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* return the division of 2 vector 4s  */
M_INLINE vec4 vec4_div(const vec4 vector_0,
                       const vec4 vector_1) {

  vec4 result;
  result.elements[0] = vector_0.elements[0] / vector_1.elements[0];
  result.elements[1] = vector_0.elements[1] / vector_1.elements[1];
  result.elements[2] = vector_0.elements[2] / vector_1.elements[2];
  result.elements[3] = vector_0.elements[3] / vector_1.elements[3];
  return result;
}
/* * * * * * * * * * * * * * * * * * * */

/* return squared length of a
 * vector 4 using the pithagorean theorem  */
M_INLINE f32 vec4_length_squared(const vec4 vector) {
  return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z + vector.w * vector.w;
}
/* * * * * * * * * * * * * * * * * * * * * */

/* return the length of a vector 4 */
M_INLINE f32 vec4_length(const vec4 vector) {
  return m_sqrt(vec4_length_squared(vector));
}
/* * * * * * * * * * * * * * * * * */

/* normalize vector 4 to a unit vector */
M_INLINE u0 vec4_normalize(vec4 * const vector) {
  const f32 length = vec4_length(*vector);
  vector->x /= length;
  vector->y /= length;
  vector->z /= length;
  vector->w /= length;
}
/* * * * * * * * * * * * * * * * * * * */

/* return a normalized copy of the vector 3  */
M_INLINE vec4 vec4_normalized(vec4 vector) {
  vec4_normalize(&vector);
  return vector;
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* return the dot product between 8 f32s */
M_INLINE f32 vec4_dot_f32(const f32 a0,
                          const f32 a1,
                          const f32 a2,
                          const f32 a3,

                          const f32 b0,
                          const f32 b1,
                          const f32 b2,
                          const f32 b3) {

  const f32 p = a0 * b0 +
                a1 * b1 +
                a2 * b2 +
                a3 * b3;
  return p;
}
/* * * * * * * * * * * * * * * * * * * * */

/* create an identity matrix */
M_INLINE mat4 mat4_identity(u0) {
  /* {
   *   {1, 0, 0, 0},
   *   {0, 1, 0, 0},
   *   {0, 0, 1, 0},
   *   {0, 0, 0, 1},
   * } */
  mat4 out_matrix;
  m_zero_memory(out_matrix.data, sizeof(f32) * 16);
  out_matrix.data[0] = 1.0f;
  out_matrix.data[0 + 5] = 1.0f;
  out_matrix.data[0 + 5 + 5] = 1.0f;
  out_matrix.data[0 + 5 + 5 + 5] = 1.0f;
  return out_matrix;
}
/* * * * * * * * * * * * * * */

/* return multiplication between 2 4x4 matrices  */
M_INLINE mat4 mat4_mul(mat4 matrix_0,
                       mat4 matrix_1) {

  mat4 out_matrix = mat4_identity();
  const f32 * m1_ptr = matrix_0.data;
  const f32 * m2_ptr = matrix_1.data;
  f32 * dst_ptr = out_matrix.data;

  for (u8 i = 0; i < 4; ++i) {
    for (u8 j = 0; j < 4; ++j) {
      *dst_ptr =
        m1_ptr[0] * m2_ptr[0  + j] +
        m1_ptr[1] * m2_ptr[4  + j] +
        m1_ptr[2] * m2_ptr[8  + j] +
        m1_ptr[3] * m2_ptr[12 + j];
      ++dst_ptr;
    }
    m1_ptr += 4;
  }
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * * * * * */

/* return orthographic projection matrix */
M_INLINE mat4 mat4_orthographic(const f32 left,
                                const f32 right,
                                const f32 bottom,
                                const f32 top,
                                const f32 near_clip,
                                const f32 far_clip) {

  mat4 out_matrix = mat4_identity();

  const f32 lr = 1.0f / (left - right);
  const f32 bt = 1.0f / (bottom - top);
  const f32 nf = 1.0f / (near_clip - far_clip);

  out_matrix.data[0] = -2.0f * lr;
  out_matrix.data[5] = -2.0f * bt;
  out_matrix.data[10] = 2.0f * nf;

  out_matrix.data[12] = (left + right) * lr;
  out_matrix.data[13] = (top + bottom) * bt;
  out_matrix.data[14] = (far_clip + near_clip) * nf;
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * */

/* return perspective projection matrix  */
M_INLINE mat4 mat4_perspective(const f32 fov_radians,
                               const f32 aspect_ratio,
                               const f32 near_clip,
                               const f32 far_clip) {

  const f32 half_tan_fov = m_tan(fov_radians * 0.5f);
  mat4 out_matrix;
  m_zero_memory(out_matrix.data, sizeof(f32) * 16);
  out_matrix.data[0] = 1.0f / (aspect_ratio * half_tan_fov);
  out_matrix.data[5] = 1.0f / half_tan_fov;
  out_matrix.data[10] = -((far_clip + near_clip) / (far_clip - near_clip));
  out_matrix.data[11] = -1.0f;
  out_matrix.data[14] = -((2.0f * far_clip * near_clip) / (far_clip - near_clip));
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * */

/* return look-at matrix */
M_INLINE mat4 mat4_look_at(const vec3 position,
                           const vec3 target,
                           const vec3 up) {

  mat4 out_matrix;
  vec3 z_axis;
  z_axis.x = target.x - position.x;
  z_axis.y = target.y - position.y;
  z_axis.z = target.z - position.z;

  z_axis      = vec3_normalized(z_axis);
  vec3 x_axis = vec3_normalized(vec3_cross(z_axis, up));
  vec3 y_axis = vec3_cross(x_axis, z_axis);

  out_matrix.data[0]  = x_axis.x;
  out_matrix.data[1]  = y_axis.x;
  out_matrix.data[2]  = -z_axis.x;
  out_matrix.data[3]  = 0;
  out_matrix.data[4]  = x_axis.y;
  out_matrix.data[5]  = y_axis.y;
  out_matrix.data[6]  = -z_axis.y;
  out_matrix.data[7]  = 0;
  out_matrix.data[8]  = x_axis.z;
  out_matrix.data[9]  = y_axis.z;
  out_matrix.data[10] = -z_axis.z;
  out_matrix.data[11] = 0;
  out_matrix.data[12] = -vec3_dot(x_axis, position);
  out_matrix.data[13] = -vec3_dot(y_axis, position);
  out_matrix.data[14] = vec3_dot(z_axis, position);
  out_matrix.data[15] = 1.0f;

  return out_matrix;
}
/* * * * * * * * * * * * */

/* return a transposed copy of the provided matrix */
M_INLINE mat4 mat4_transposed(mat4 matrix) {
  mat4 out_matrix = mat4_identity();
  out_matrix.data[0]  = matrix.data[0];
  out_matrix.data[1]  = matrix.data[4];
  out_matrix.data[2]  = matrix.data[8];
  out_matrix.data[3]  = matrix.data[12];
  out_matrix.data[4]  = matrix.data[1];
  out_matrix.data[5]  = matrix.data[5];
  out_matrix.data[6]  = matrix.data[9];
  out_matrix.data[7]  = matrix.data[13];
  out_matrix.data[8]  = matrix.data[2];
  out_matrix.data[9]  = matrix.data[6];
  out_matrix.data[10] = matrix.data[10];
  out_matrix.data[11] = matrix.data[14];
  out_matrix.data[12] = matrix.data[3];
  out_matrix.data[13] = matrix.data[7];
  out_matrix.data[14] = matrix.data[11];
  out_matrix.data[15] = matrix.data[15];
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * */

/* return an inverse matrix  */
M_INLINE mat4 mat4_inverse(mat4 matrix) {
  const f32 * m = matrix.data;

  f32 t0  = m[10] * m[15];
  f32 t1  = m[14] * m[11];
  f32 t2  = m[6]  * m[15];
  f32 t3  = m[14] * m[7];
  f32 t4  = m[6]  * m[11];
  f32 t5  = m[10] * m[7];
  f32 t6  = m[2]  * m[15];
  f32 t7  = m[14] * m[3];
  f32 t8  = m[2]  * m[11];
  f32 t9  = m[10] * m[3];
  f32 t10 = m[2]  * m[7];
  f32 t11 = m[6]  * m[3];
  f32 t12 = m[8]  * m[13];
  f32 t13 = m[12] * m[9];
  f32 t14 = m[4]  * m[13];
  f32 t15 = m[12] * m[5];
  f32 t16 = m[4]  * m[9];
  f32 t17 = m[8]  * m[5];
  f32 t18 = m[0]  * m[13];
  f32 t19 = m[12] * m[1];
  f32 t20 = m[0]  * m[9];
  f32 t21 = m[8]  * m[1];
  f32 t22 = m[0]  * m[5];
  f32 t23 = m[4]  * m[1];

  mat4 out_matrix;
  f32 * o = out_matrix.data;

  o[0] = (t0 * m[5] + t3 * m[9] + t4  * m[13]) - (t1 * m[5] + t2 * m[9] + t5  * m[13]);
  o[1] = (t1 * m[1] + t6 * m[9] + t9  * m[13]) - (t0 * m[1] + t7 * m[9] + t8  * m[13]);
  o[2] = (t2 * m[1] + t7 * m[5] + t10 * m[13]) - (t3 * m[1] + t6 * m[5] + t11 * m[13]);
  o[3] = (t5 * m[1] + t8 * m[5] + t11 * m[9])  - (t4 * m[1] + t9 * m[5] + t10 * m[9]);

  f32 d = (1.0f / (m[0] * o[0] + m[4] * o[1] + m[8] * o[2] + m[12] * o[3]));

  o[0]  = d * o[0];
  o[1]  = d * o[1];
  o[2]  = d * o[2];
  o[3]  = d * o[3];
  o[4]  = d * ((t1  * m[4]  + t2  * m[8]  + t5  * m[12]) - (t0  * m[4]  + t3   * m[8]  + t4  * m[12]));
  o[5]  = d * ((t0  * m[0]  + t7  * m[8]  + t8  * m[12]) - (t1  * m[0]  + t6   * m[8]  + t9  * m[12]));
  o[6]  = d * ((t3  * m[0]  + t6  * m[4]  + t11 * m[12]) - (t2  * m[0]  + t7   * m[4]  + t10 * m[12]));
  o[7]  = d * ((t4  * m[0]  + t9  * m[4]  + t10 * m[8])  - (t5  * m[0]  + t8   * m[4]  + t11 * m[8]));
  o[8]  = d * ((t12 * m[7]  + t15 * m[11] + t16 * m[15]) - (t13 * m[7]  + t14  * m[11] + t17 * m[15]));
  o[9]  = d * ((t13 * m[3]  + t18 * m[11] + t21 * m[15]) - (t12 * m[3]  + t19  * m[11] + t20 * m[15]));
  o[10] = d * ((t14 * m[3]  + t19 * m[7]  + t22 * m[15]) - (t15 * m[3]  + t18  * m[7]  + t23 * m[15]));
  o[11] = d * ((t17 * m[3]  + t20 * m[7]  + t23 * m[11]) - (t16 * m[3]  + t21  * m[7]  + t22 * m[11]));
  o[12] = d * ((t14 * m[10] + t17 * m[14] + t13 * m[6])  - (t16 * m[14] + t12  * m[6]  + t15 * m[10]));
  o[13] = d * ((t20 * m[14] + t12 * m[2]  + t19 * m[10]) - (t18 * m[10] + t21  * m[14] + t13 * m[2]));
  o[14] = d * ((t18 * m[6]  + t23 * m[14] + t15 * m[2])  - (t22 * m[14] + t14  * m[2]  + t19 * m[6]));
  o[15] = d * ((t22 * m[10] + t16 * m[2]  + t21 * m[6])  - (t20 * m[6]  + t23  * m[10] + t17 * m[2]));

  return out_matrix;
}
/* * * * * * * * * * * * * * */

/* return a 4x4 translation matrix */
M_INLINE mat4 mat4_translation(const vec3 position) {
  mat4 out_matrix = mat4_identity();
  out_matrix.data[12] = position.x;
  out_matrix.data[13] = position.y;
  out_matrix.data[14] = position.z;
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * */

/* return a 4x4 scale matrix */
M_INLINE mat4 mat4_scale(const vec3 scale) {
  mat4 out_matrix = mat4_identity();
  out_matrix.data[0]  = scale.x;
  out_matrix.data[5]  = scale.y;
  out_matrix.data[10] = scale.z;
  return out_matrix;
}
/* * * * * * * * * * * * * * */

/* return 4x4 matrix from `x` euler angle  */
M_INLINE mat4 mat4_euler_x(const f32 angle_radians) {
  mat4 out_matrix = mat4_identity();
  const f32 c     = m_cos(angle_radians);
  const f32 s     = m_sin(angle_radians);

  out_matrix.data[5]  =  c;
  out_matrix.data[6]  =  s;
  out_matrix.data[9]  = -s;
  out_matrix.data[10] =  c;
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * * */

/* return 4x4 matrix from `y` euler angle  */
M_INLINE mat4 mat4_euler_y(const f32 angle_radians) {
  mat4 out_matrix = mat4_identity();
  const f32 c     = m_cos(angle_radians);
  const f32 s     = m_sin(angle_radians);

  out_matrix.data[0]  =  c;
  out_matrix.data[2]  = -s;
  out_matrix.data[8]  =  s;
  out_matrix.data[10] =  c;
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * * */

/* return 4x4 matrix from `z` euler angle  */
M_INLINE mat4 mat4_euler_z(const f32 angle_radians) {
  mat4 out_matrix = mat4_identity();

  const f32 c = m_cos(angle_radians);
  const f32 s = m_sin(angle_radians);

  out_matrix.data[0] =  c;
  out_matrix.data[1] =  s;
  out_matrix.data[4] = -s;
  out_matrix.data[5] =  c;
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * * */

/* return 4x4 matrix from `xyz` euler angles */
M_INLINE mat4 mat4_euler_xyz(const f32 x_radians,
                             const f32 y_radians,
                             const f32 z_radians) {

  const mat4 rx   = mat4_euler_x(x_radians);
  const mat4 ry   = mat4_euler_y(y_radians);
  const mat4 rz   = mat4_euler_z(z_radians);
  mat4 out_matrix = mat4_mul(rx, ry);
  out_matrix      = mat4_mul(out_matrix, rz);
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* return a forward vector 3 relative to the provided matrix */
M_INLINE vec3 mat4_forward(const mat4 matrix) {
  vec3 forward;
  forward.x = -matrix.data[2];
  forward.y = -matrix.data[6];
  forward.z = -matrix.data[10];
  vec3_normalize(&forward);
  return forward;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return a backwards vector 3 relative to the provided matrix */
M_INLINE vec3 mat4_backward(const mat4 matrix) {
  vec3 backward;
  backward.x = matrix.data[2];
  backward.y = matrix.data[6];
  backward.z = matrix.data[10];
  vec3_normalize(&backward);
  return backward;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return an upwards vector 3 relative to the provided matrix  */
M_INLINE vec3 mat4_up(const mat4 matrix) {
  vec3 up;
  up.x = matrix.data[1];
  up.y = matrix.data[5];
  up.z = matrix.data[9];
  vec3_normalize(&up);
  return up;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return a downwards vector 3 relative to the provided matrix */
M_INLINE vec3 mat4_down(const mat4 matrix) {
  vec3 down;
  down.x = -matrix.data[1];
  down.y = -matrix.data[5];
  down.z = -matrix.data[9];
  vec3_normalize(&down);
  return down;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return a leftwards vector 3 relative to the provided matrix */
M_INLINE vec3 mat4_left(const mat4 matrix) {
  vec3 right;
  right.x = -matrix.data[0];
  right.y = -matrix.data[4];
  right.z = -matrix.data[8];
  vec3_normalize(&right);
  return right;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return a rightwards vector 3 relative to the provided matrix  */
M_INLINE vec3 mat4_right(const mat4 matrix) {
  vec3 left;
  left.x = matrix.data[0];
  left.y = matrix.data[4];
  left.z = matrix.data[8];
  vec3_normalize(&left);
  return left;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return an identity quaternion */
M_INLINE quat quat_identity(u0) {
  return (quat){0, 0, 0, 1.0f};
}
/* * * * * * * * * * * * * * * * */

/* return the normal of a quaternion */
M_INLINE f32 quat_normal(const quat q) {
  return m_sqrt(q.x * q.x +
                q.y * q.y +
                q.z * q.z +
                q.w * q.w );
}
/* * * * * * * * * * * * * * * * * * */

/* return normalized quaternion  */
M_INLINE quat quat_normalize(const quat q) {
  const f32 normal = quat_normal(q);
  return (quat){
           q.x / normal,
           q.y / normal,
           q.z / normal,
           q.w / normal,
         };
}
/* * * * * * * * * * * * * * * * */

/* return a quaternions conjugate  */
M_INLINE quat quat_conjugate(const quat q) {
  return (quat){
           -q.x,
           -q.y,
           -q.z,
            q.w,
         };
}
/* * * * * * * * * * * * * * * * * */

/* return the quaternion inverse */
M_INLINE quat quat_inverse(const quat q) {
  return quat_normalize(quat_conjugate(q));
}
/* * * * * * * * * * * * * * * * */

/* return the multiplication between 2 quaternions */
M_INLINE quat quat_mul(const quat q_0,
                       const quat q_1) {

  quat out_quaternion;

  out_quaternion.x =  q_0.x * q_1.w +
                      q_0.y * q_1.z -
                      q_0.z * q_1.y +
                      q_0.w * q_1.x;

  out_quaternion.y = -q_0.x * q_1.z +
                      q_0.y * q_1.w +
                      q_0.z * q_1.x +
                      q_0.w * q_1.y;

  out_quaternion.z =  q_0.x * q_1.y -
                      q_0.y * q_1.x +
                      q_0.z * q_1.w +
                      q_0.w * q_1.z;

  out_quaternion.w = -q_0.x * q_1.x -
                      q_0.y * q_1.y -
                      q_0.z * q_1.z +
                      q_0.w * q_1.w;

  return out_quaternion;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * */

/* return the dot product between 2 quaternions  */
M_INLINE f32 quat_dot(const quat q_0,
                      const quat q_1) {

  return q_0.x * q_1.x +
         q_0.y * q_1.y +
         q_0.z * q_1.z +
         q_0.w * q_1.w;
}
/* * * * * * * * * * * * * * * * * * * * * * * * */

/* return 4x4 matrix from quaternion */
M_INLINE mat4 quat_to_mat4(const quat q) {
  mat4 out_matrix = mat4_identity();

  /*
   * https://stackoverflow.com/questions/1556260/convert-quaternion-rotation-to-rotation-matrix
   */

  quat n = quat_normalize(q);

  out_matrix.data[0]  = 1.0f - 2.0f * n.y * n.y  - 2.0f * n.z * n.z;
  out_matrix.data[1]  = 2.0f * n.x  * n.y - 2.0f * n.z  * n.w;
  out_matrix.data[2]  = 2.0f * n.x  * n.z + 2.0f * n.y  * n.w;

  out_matrix.data[4]  = 2.0f * n.x  * n.y + 2.0f * n.z  * n.w;
  out_matrix.data[5]  = 1.0f - 2.0f * n.x * n.x  - 2.0f * n.z * n.z;
  out_matrix.data[6]  = 2.0f * n.y  * n.z - 2.0f * n.x  * n.w;

  out_matrix.data[8]  = 2.0f * n.x  * n.z - 2.0f * n.y  * n.w;
  out_matrix.data[9]  = 2.0f * n.y  * n.z + 2.0f * n.x  * n.w;
  out_matrix.data[10] = 1.0f - 2.0f * n.x * n.x  - 2.0f * n.y * n.y;

  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * */

/* return 4x4 rotation matrix from quaternion  */
M_INLINE mat4 quat_to_rotation_matrix(const quat q,
                                      const vec3 center) {

  mat4 out_matrix;

  f32 * o = out_matrix.data;
  o[0]  = (q.x * q.x) - (q.y * q.y) - (q.z * q.z) + (q.w * q.w);
  o[1]  = 2.0f * ((q.x * q.y) + (q.z * q.w));
  o[2]  = 2.0f * ((q.x * q.z) - (q.y * q.w));
  o[3]  = center.x - center.x * o[0] - center.y * o[1] - center.z * o[2];

  o[4]  = 2.0f * ((q.x * q.y) - (q.z * q.w));
  o[5]  = -(q.x * q.x) + (q.y * q.y) - (q.z * q.z) + (q.w * q.w);
  o[6]  = 2.0f * ((q.y * q.z) + (q.x * q.w));
  o[7]  = center.y - center.x * o[4] - center.y * o[5] - center.z * o[6];

  o[8]  = 2.0f * ((q.x * q.z) + (q.y * q.w));
  o[9]  = 2.0f * ((q.y * q.z) - (q.x * q.w));
  o[10] = -(q.x * q.x) - (q.y * q.y) + (q.z * q.z) + (q.w * q.w);
  o[11] = center.z - center.x * o[8] - center.y * o[9] - center.z * o[10];

  o[12] = 0.0f;
  o[13] = 0.0f;
  o[14] = 0.0f;
  o[15] = 1.0f;
  return out_matrix;
}
/* * * * * * * * * * * * * * * * * * * * * * * */

/* return quaternion from euler angle  */
M_INLINE quat quat_from_axis_angle(const vec3 axis,
                                   const f32 angle,
                                   const b8 normalize) {

  const f32 half_angle = (0.5f * angle);
  const f32 s = m_sin(half_angle);
  const f32 c = m_cos(half_angle);

  const quat q = (quat){
    s * axis.x,
    s * axis.y,
    s * axis.z,
    c
  };
  if (normalize) {
      return quat_normalize(q);
  }
  return q;
}
/* * * * * * * * * * * * * * * * * * * */

/* return smooth linear interpolated quaternion */
M_INLINE quat quat_slerp(const quat q_0,
                         const quat q_1,
                         const f32 percentage) {

  /* source: https://en.wikipedia.org/wiki/Slerp
     only unit quaternions are valid rotations
     normalize to avoid undefined behavior */
  quat out_quaternion;
  const quat v0 = quat_normalize(q_0);
  quat v1 = quat_normalize(q_1);

  /* compute the cosine of the angle between the two vectors */
  f32 dot = quat_dot(v0, v1);

  /* If the dot product is negative, slerp won't take
     the shorter path. Note that v1 and -v1 are equivalent when
     the negation is applied to all four components. Fix by
     reversing one quaternion. */
  if (dot < 0.0f) {
      v1.x = -v1.x;
      v1.y = -v1.y;
      v1.z = -v1.z;
      v1.w = -v1.w;
      dot = -dot;
  }

  const f32 DOT_THRESHOLD = 0.9995f;
  if (dot > DOT_THRESHOLD) {
      /* If the inputs are too close for comfort,
         linearly interpolate
         and normalize the result. */
      out_quaternion = (quat){
          v0.x + ((v1.x - v0.x) * percentage),
          v0.y + ((v1.y - v0.y) * percentage),
          v0.z + ((v1.z - v0.z) * percentage),
          v0.w + ((v1.w - v0.w) * percentage)};

      return quat_normalize(out_quaternion);
  }

  /* Since dot is in range [0, DOT_THRESHOLD], acos is safe */
  const f32 theta_0     = m_acos(dot);          /* theta_0 = angle between input vectors */
  const f32 theta       = theta_0 * percentage; /* theta = angle between v0 and result */
  const f32 sin_theta   = m_sin(theta);         /* compute this value only once */
  const f32 sin_theta_0 = m_sin(theta_0);       /* compute this value only once */

  const f32 s0 = (m_cos(theta) - dot * sin_theta / sin_theta_0); /* == sin(theta_0 - theta) / sin(theta_0) */
  const f32 s1 = (sin_theta / sin_theta_0);

  return (quat){
           (v0.x * s0) + (v1.x * s1),
           (v0.y * s0) + (v1.y * s1),
           (v0.z * s0) + (v1.z * s1),
           (v0.w * s0) + (v1.w * s1)
         };
}
/* * * * * * * * * * * * * * * *  * * * * * * * */

/* return value converted to radians */
M_INLINE f32 deg_to_rad(const f32 degrees) {
  return (degrees * M_DEG2RAD_MULTIPLIER);
}
/* * * * * * * * * * * * * * * * * * */

/* return value converted to degrees */
M_INLINE f32 rad_to_deg(const f32 radians) {
  return (radians * M_RAD2DEG_MULTIPLIER);
}
/* * * * * * * * * * * * * * * * * * */
