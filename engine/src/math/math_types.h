#pragma once

#include <defines.h>

/* vector of 2 components  */
typedef union vec2_u {
  f32 elements[2];
  struct {
    union {
      /* first element */
      f32 x, r, s, u;
    };
    union {
      /* second element */
      f32 y, g, t, v;
    };
  };
} vec2;
/* * * * * * * * * * * * * */

/* vector of 3 components  */
typedef union vec3_u {
  f32 elements[3];
  struct {
    union {
      /* first element */
      f32 x, r, s, u;
    };
    union {
      /* second element */
      f32 y, g, t, v;
    };
    union {
      /* third element */
      f32 z, b, p, w;
    };
  };
} vec3;
/* * * * * * * * * * * * * */

/* vector of 4 components  */
typedef union vec4_u {
  f32 elements[4];
  struct {
    union {
      /* first element */
      f32 x, r, s;
    };
    union {
      /* second element */
      f32 y, g, t;
    };
    union {
      /* third element */
      f32 z, b, p;
    };
    union {
      /* fourth element */
      f32 w, a, q;
    };
  };
} vec4;
/* * * * * * * * * * * * * */

/* quaternions */
typedef vec4 quat;
/* * * * * * * */

/* 4x4 matrix  */
typedef union mat4_u {
  f32 data[16];
} mat4;
/* * * * * * * */
