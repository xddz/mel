#include "m_math.h"
#include <platform/platform.h>

#include <math.h>
#include <stdlib.h>

static b8 rand_seeded = false;

/* return the sine value of `x`  */
f32 m_sin(const f32 x) {
    return sinf(x);
}
/* * * * * * * * * * * * * * * * */

/* return the cosine value of `x`  */
f32 m_cos(const f32 x) {
    return cosf(x);
}
/* * * * * * * * * * * * * * * * * */

/* return the tangent value of `x` */
f32 m_tan(const f32 x) {
    return tanf(x);
}
/* * * * * * * * * * * * * * * * * */

/* return the arc cosine value of `x`  */
f32 m_acos(const f32 x) {
    return acosf(x);
}
/* * * * * * * * * * * * * * * * * * * */

/* return the square root of `x` */
f32 m_sqrt(const f32 x) {
    return sqrtf(x);
}
/* * * * * * * * * * * * * * * * */

/* return the absolute value of `x`  */
f32 m_abs(const f32 x) {
    return fabsf(x);
}
/* * * * * * * * * * * * * * * * * * */

/* return an s32 random value */
s32 m_random(u0) {
    if (!rand_seeded) {
        srand((u32)platform_get_absolute_time());
        rand_seeded = true;
    }
    return rand();
}
/* * * * * * * * * * * * * * * */

/* return an s32 random value in the provided range  */
s32 m_random_in_range(const s32 min,
                      const s32 max) {

    if (!rand_seeded) {
        srand((u32)platform_get_absolute_time());
        rand_seeded = true;
    }
    return (rand() % (max - min + 1)) + min;
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * */

/* return an f32 random value  */
f32 m_frandom(u0) {
    return (float)m_random() / (f32)RAND_MAX;
}
/* * * * * * * * * * * * * * * */

/* return an f32 random value in the provided range  */
f32 m_frandom_in_range(const f32 min,
                       const f32 max) {

    return min + ((float)m_random() / ((f32)RAND_MAX / (max - min)));
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * */
