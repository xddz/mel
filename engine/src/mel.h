#pragma once

#include "./core/application.h"

/* basic application state representation  */
typedef struct mel_t {
  /* application configuration */
  application_config app_config;
  /* * * * * * * * * * * * * * */

  /* application initialization function */
  b8 (*initialize)(struct mel_t * ctx);
  /* * * * * * * * * * * * * * * * * * * */

  /* application update function */
  b8 (*update)(struct mel_t * ctx, const f32 delta_time);
  /* * * * * * * * * * * * * * * */

  /* application render function */
  b8 (*render)(struct mel_t * ctx, const f32 delta_time);
  /* * * * * * * * * * * * * * * */

  /* application resize callback */
  u0 (*on_resize)(struct mel_t * ctx, const u16 width, const u16 height);
  /* * * * * * * * * * * * * * * */

  /* application specific state
   * created and managed by the application  */
  u0 * state;
  /* * * * * * * * * * * * * * * * * * * * * */
} mel_t;
/* * * * * * * * * * * * * * * * * * * * * */
