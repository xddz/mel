#pragma once

#include "./core/application.h"
#include "./core/logger.h"
#include "./core/m_memory.h"
#include "./mel.h"

extern b8 mel(mel_t * ctx);

/* the main entry point 
   of the application  */
s32 main(u0) {

  initialize_memory();

  mel_t ctx;
  if (!mel(&ctx)){
    M_FATAL("Failed initializing.");
    return -1;
  }

  if (!ctx.render || !ctx.update || !ctx.initialize || !ctx.on_resize){
    M_FATAL("Unassigned callbacks.");
    return -2;
  }

  if (!application_create(&ctx)){
    M_INFO("Failed creating application.");
    return 1;
  }

  /* beginning of game-loop  */
  if (!application_run()){
    M_INFO("Application did not shutdown correctly.");
    return 2;
  }
  /* * * * * * * * * * * * * */

  shutdown_memory();

  return 0;
}
/* * * * * * * * * * * */
