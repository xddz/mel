#include "renderer_frontend.h"
#include "renderer_backend.h"
#include "core/logger.h"
#include "core/m_memory.h"

struct renderer_backend * backend = 0;

b8 renderer_initialize(const char * const title,
                       struct platform_state * platform) {

  backend = m_allocate(sizeof(renderer_backend), MEMORY_TAG_RENDERER);

  /* TODO: make this configurable */
  renderer_backend_create(RENDERER_BACKEND_TYPE_VULKAN, platform, backend);
  backend->frame_number = 0;

  if (!backend->initialize(backend, title, platform)) {
    M_FATAL("Failed initializing Renderer backend of type: [%s].", str(RENDERER_BACKEND_TYPE_VULKAN));
    return false;
  }
  return true;
}

u0 renderer_shutdown(u0) {
  backend->shutdown(backend);
  m_free(backend, sizeof(renderer_backend), MEMORY_TAG_RENDERER);
}

b8 renderer_begin_frame(const f32 delta_time) {
  return backend->begin_frame(backend, delta_time);
}

b8 renderer_end_frame(const f32 delta_time) {
  b8 result = backend->end_frame(backend, delta_time);
  ++backend->frame_number;
  return result;
}

u0 renderer_on_resized(const u16 width, const u16 height) {
  if (backend) {
    backend->resized(backend,
                     width,
                     height);
    return;
  }
  M_WARN("Renderer backend does not exist and cannot accept resize: %dx%d", width, height);
}

b8 renderer_draw_frame(render_packet * packet) {
  if (renderer_begin_frame(packet->delta_time)) {

    if (!renderer_end_frame(packet->delta_time)) {
      M_ERROR("%s failed.", str(renderer_end_frame));
      return false;
    }
  }
  return true;
}
