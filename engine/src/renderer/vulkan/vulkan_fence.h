#pragma once

#include <defines.h>
#include "vulkan_types.h"

u0 vulkan_fence_create(vulkan_context * context,
                         const b8 create_signaled,
                         vulkan_fence * out_fence);

u0 vulkan_fence_destroy(vulkan_context * context,
                          vulkan_fence * fence);

b8 vulkan_fence_wait(vulkan_context * context,
                     vulkan_fence * fence,
                     const u64 timeout_ns);

u0 vulkan_fence_reset(vulkan_context * context,
                        vulkan_fence * fence);
