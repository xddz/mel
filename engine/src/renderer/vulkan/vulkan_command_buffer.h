#pragma once
#include <defines.h>
#include "vulkan_types.h"

u0 vulkan_command_buffer_allocate(vulkan_context * context,
                                    VkCommandPool pool,
                                    b8 is_primary,
                                    vulkan_command_buffer * out_command_buffer);

u0 vulkan_command_buffer_free(vulkan_context * context,
                                VkCommandPool pool,
                                vulkan_command_buffer * command_buffer);

u0 vulkan_command_buffer_begin(vulkan_command_buffer * command_buffer,
                                 b8 is_single_use,
                                 b8 is_renderpass_continue,
                                 b8 is_simultaneous_use);

u0 vulkan_command_buffer_end(vulkan_command_buffer * command_buffer);

u0 vulkan_command_buffer_update_submitted(vulkan_command_buffer * command_buffer);

u0 vulkan_command_buffer_reset(vulkan_command_buffer * command_buffer);

/* automatically allocates and starts recording */
u0 vulkan_command_buffer_begin_single_use(vulkan_context * context,
                                            VkCommandPool pool,
                                            vulkan_command_buffer * out_command_buffer);

/* automatically ends and submits recording */
u0 vulkan_command_buffer_end_single_use(vulkan_context * context,
                                          VkCommandPool pool,
                                          vulkan_command_buffer * command_buffer,
                                          VkQueue queue);
