#include "vulkan_backend.h"

#include "vulkan_types.h"
#include "vulkan_platform.h"
#include "vulkan_device.h"
#include "vulkan_swapchain.h"
#include "vulkan_renderpass.h"
#include "vulkan_command_buffer.h"
#include "vulkan_framebuffer.h"
#include "vulkan_fence.h"
#include "vulkan_utils.h"

#include <core/application.h>
#include <core/logger.h>
#include <core/m_string.h>
#include <core/m_memory.h>

#include <containers/darray.h>

#include <platform/platform.h>

static vulkan_context context;
static u16 cached_framebuffer_width  = 0;
static u16 cached_framebuffer_height = 0;

VKAPI_ATTR VkBool32 VKAPI_CALL vk_debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
                                                 VkDebugUtilsMessageTypeFlagsEXT message_types,
                                                 const VkDebugUtilsMessengerCallbackDataEXT * data,
                                                 u0 * user_data);

s32 find_memory_index(u32 type_filter, u32 property_flags);

u0 create_command_buffers(renderer_backend * backend);

u0 regenerate_framebuffers(renderer_backend * backend,
                             vulkan_swapchain * swapchain,
                             vulkan_renderpass * renderpass);

b8 recreate_swapchain(renderer_backend * backend);

b8 vulkan_renderer_backend_initialize(struct renderer_backend * backend,
                                      const char * const title,
                                      struct platform_state * plat_state) {

  context.find_memory_index = find_memory_index;

  /* TODO: custom allocator */
  context.allocator = 0;

  application_get_framebuffer_size(&cached_framebuffer_width, &cached_framebuffer_height);
  context.framebuffer_width  = ((cached_framebuffer_width != 0) ? cached_framebuffer_width : 800);
  context.framebuffer_height = ((cached_framebuffer_height != 0) ? cached_framebuffer_height : 600);
  cached_framebuffer_width   = 0;
  cached_framebuffer_height  = 0;
  
  /* initialize vulkan instance  */
  VkApplicationInfo app_info          = {VK_STRUCTURE_TYPE_APPLICATION_INFO};
  app_info.apiVersion                 = VK_API_VERSION_1_2;
  app_info.pApplicationName           = title;
  app_info.applicationVersion         = VK_MAKE_VERSION(1, 0, 0);
  app_info.pEngineName                = "mel";
  app_info.engineVersion              = VK_MAKE_VERSION(1, 0, 0);

  VkInstanceCreateInfo create_info    = {VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO};
  create_info.pApplicationInfo        = &app_info;

  /* obtain a list of required extensions  */
  const char ** required_extensions = darray_create(const char *);
  darray_push(required_extensions, &VK_KHR_SURFACE_EXTENSION_NAME);     /* generic surface extension   */
  platform_get_required_extension_names(&required_extensions);          /* platform-specific extension */
  /* * * * * * * * * * * * * * * * * * * * */

#ifdef _DEBUG
  darray_push(required_extensions, &VK_EXT_DEBUG_UTILS_EXTENSION_NAME); /* debug utilities             */

  M_DEBUG("Required extensions:");
  u32 length = darray_length(required_extensions);
  for (u32 i = 0; i < length; ++i) {
    M_DEBUG(required_extensions[i]);
  }
#endif

  create_info.enabledExtensionCount   = darray_length(required_extensions);
  create_info.ppEnabledExtensionNames = required_extensions;

  /* validation layers */
  const char ** required_validation_layer_names = 0;
  u32 required_validation_layer_count           = 0;

  /* validation layers should only be enabled on non-release builds */
#ifdef _DEBUG
  M_DEBUG("Validation Layers enabled.");

  required_validation_layer_names = darray_create(const char *);
  darray_push(required_validation_layer_names, &"VK_LAYER_KHRONOS_validation");
  required_validation_layer_count = darray_length(required_validation_layer_names);

  /* obtain list of available validation layers  */
  u32 available_layer_count = 0;
  VK_CHECK(vkEnumerateInstanceLayerProperties(&available_layer_count, 0));
  VkLayerProperties * available_layers = darray_reserve(VkLayerProperties, available_layer_count);
  VK_CHECK(vkEnumerateInstanceLayerProperties(&available_layer_count, available_layers));
  /* * * * * * * * * * * * * * * * * * * * * * * */

  /* verify whether all required layers are available  */
  for (u32 i = 0; i < required_validation_layer_count; ++i) {
    M_DEBUG("Searching for Layer: %s...", required_validation_layer_names[i]);
    b8 found = false;
    for (u32 j = 0; j < available_layer_count; ++j) {
      if (strings_equal(required_validation_layer_names[i], available_layers[j].layerName)) {
        found = true;
        M_DEBUG("Found.");
        break;
      }
    }

    if (!found) {
      M_FATAL("Required Validation Layer is missing: %s", required_validation_layer_names[i]);
      return false;
    }
  }
  M_DEBUG("All required Validation Layers are present.");
  /* * * * * * * * * * * * * * * * * * * * * * * * * * */
#endif

  create_info.enabledLayerCount                 = required_validation_layer_count;
  create_info.ppEnabledLayerNames               = required_validation_layer_names;
  /* * * * * * * * * * */

  /* Vulkan debugger */
#ifdef _DEBUG
  M_DEBUG("Creating Vulkan Debugger...");
  u32 log_level = VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT |
                  VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT; /* |
                  VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT        |
                  VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT; */
  VkDebugUtilsMessengerCreateInfoEXT debug_create_info = 
                       {VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT};
  debug_create_info.messageSeverity = log_level;
  debug_create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                                  VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT |
                                  VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
  debug_create_info.pfnUserCallback = vk_debug_callback;
  create_info.pNext = (const u0 *)&debug_create_info;
#endif

  VK_CHECK(vkCreateInstance(&create_info, context.allocator, &context.instance));
  M_INFO("Successfully created Vulkan Instance.");

#ifdef _DEBUG
  PFN_vkCreateDebugUtilsMessengerEXT func = 
    (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(context.instance,
                                                              "vkCreateDebugUtilsMessengerEXT");
  M_ASSERT_MSG(func, "Failed to create Debug Messenger.");
  VK_CHECK(func(context.instance, &debug_create_info, context.allocator, &context.debug_messenger));
  M_DEBUG("Vulkan Debugger created.");
#endif
  /* * * * * * * * * */

#ifdef _DEBUG
  darray_destroy(required_validation_layer_names);
#endif
  darray_destroy(required_extensions);

  /* surface creation  */
  M_DEBUG("Creating Vulkan Platform Surface.");
  if (!platform_create_vulkan_surface(plat_state, &context)) {
    M_FATAL("Failed creating Platform Surface.");
    return false;
  }
  M_DEBUG("Successfully created Vulkan Platform Surface.");
  /* * * * * * * * * * */

  /* device creation */
  if (!vulkan_device_create(&context)) {
    M_FATAL("Failed creating Vulkan Device.");
    return false;
  }
  /* * * * * * * * * */

  /* swapchain creation  */
  vulkan_swapchain_create(&context,
                          context.framebuffer_width,
                          context.framebuffer_height,
                          &context.swapchain);
  /* * * * * * * * * * * */

  /* render pass creation  */
  vulkan_renderpass_create(&context,
                           &context.main_renderpass,
                           0, 0, context.framebuffer_width, context.framebuffer_height, /* [x, y, w, h] */
                           0.0f, 0.0f, 0.2f, 1.0f,                                      /* [r, g, b, a] */
                           1.0f,                                                        /* depth range  */
                           0);                                                          /* stencil      */
  /* * * * * * * * * * * * */

  /* swapchain framebuffers  */
  context.swapchain.framebuffers = darray_reserve(vulkan_framebuffer,
                                                  context.swapchain.image_count);
  regenerate_framebuffers(backend,
                          &context.swapchain,
                          &context.main_renderpass);
  /* * * * * * * * * * * * * */

  /* command buffer creation */
  create_command_buffers(backend);
  /* * * * * * * * * * * * * */

  /* sync objects init */
  /* semaphore objects */
  context.image_available_semaphores = darray_reserve(VkSemaphore,
                                                      context.swapchain.max_frames_in_flight);
  context.queue_complete_semaphores  = darray_reserve(VkSemaphore,
                                                      context.swapchain.max_frames_in_flight);
  /* * * * * * * * * * */

  /* fence objects */
  context.in_flight_fences = darray_reserve(vulkan_fence,
                                            context.swapchain.max_frames_in_flight);
  /* * * * * * * * */
  /* * * * * * * * * * * * */

  /* sync objects creation */
  for (u8 i = 0; i < context.swapchain.max_frames_in_flight; ++i) {
    VkSemaphoreCreateInfo semaphore_create_info = {VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
    VK_CHECK(vkCreateSemaphore(context.device.logical_device,
                               &semaphore_create_info,
                               context.allocator,
                               &context.image_available_semaphores[i]));
    VK_CHECK(vkCreateSemaphore(context.device.logical_device,
                               &semaphore_create_info,
                               context.allocator,
                               &context.queue_complete_semaphores[i]));

    /* create fence in `signaled` state, indicating
     * that the first frame has already been "rendered".
     * this will prevent the program from
     * waiting indefinitely for the first frame to render since
     * it cannot be rendered until a frame is rendered before it */
    vulkan_fence_create(&context, true, &context.in_flight_fences[i]);
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  }

  /* in-flight fences should not yet exist
   * these are stored in pointers,
   * therefore, actual fences are not owned by this list */
  context.images_in_flight = darray_reserve(vulkan_fence, context.swapchain.image_count);
  for (u32 i = 0; i < context.swapchain.image_count; ++i) {
    context.images_in_flight[i] = 0;
  }
  /* * * * * * * * * * * * * * * * * * * * * * * * * * * */

  /* * * * * * * * * * * * */

  M_INFO("Successfully initialized Vulkan Renderer.");
  return true;
  /* * * * * * * * * * * * * * * */
}

u0 vulkan_renderer_backend_shutdown(struct renderer_backend * backend) {
  vkDeviceWaitIdle(context.device.logical_device);

  /* Sync Objects  */
  M_DEBUG("Destroying Vulkan Sync Objects.");
  for (u8 i = 0; i < context.swapchain.max_frames_in_flight; ++i) {
    if (context.image_available_semaphores[i]) {
      vkDestroySemaphore(context.device.logical_device,
                         context.image_available_semaphores[i],
                         context.allocator);
      context.image_available_semaphores[i] = 0;
    }
    if (context.queue_complete_semaphores[i]) {
      vkDestroySemaphore(context.device.logical_device,
                         context.queue_complete_semaphores[i],
                         context.allocator);
      context.queue_complete_semaphores[i] = 0;
    }
    vulkan_fence_destroy(&context,
                         &context.in_flight_fences[i]);
  }

  darray_destroy(context.image_available_semaphores);
  context.image_available_semaphores = 0;

  darray_destroy(context.queue_complete_semaphores);
  context.queue_complete_semaphores = 0;

  darray_destroy(context.in_flight_fences);
  context.in_flight_fences = 0;

  darray_destroy(context.images_in_flight);
  context.images_in_flight = 0;
  /* * * * * * * * */

  /* Command Buffers */
  M_DEBUG("Destroying Vulkan Command Buffers.");
  for (u32 i = 0; i < context.swapchain.image_count; ++i) {
    if (context.graphics_command_buffers[i].handle) {
      vulkan_command_buffer_free(&context,
                                 context.device.graphics_command_pool,
                                 &context.graphics_command_buffers[i]);
      context.graphics_command_buffers[i].handle = 0;
    }
  }
  darray_destroy(context.graphics_command_buffers);
  context.graphics_command_buffers = 0;
  /* * * * * * * * * */

  /* Framebuffers  */
  M_DEBUG("Destroying Vulkan Framebuffers.");
  for (u32 i = 0; i < context.swapchain.image_count; ++i) {
    vulkan_framebuffer_destroy(&context,
                               &context.swapchain.framebuffers[i]);
  }
  /* * * * * * * * */

  /* Renderpass  */
  M_DEBUG("Destroying Vulkan RenderPass.");
  vulkan_renderpass_destroy(&context, &context.main_renderpass);
  /* * * * * * * */

  /* Swapchain */
  M_DEBUG("Destroying Vulkan Swapchain.");
  vulkan_swapchain_destroy(&context, &context.swapchain);
  /* * * * * * */

  /* Vulkan Surface  */
  M_DEBUG("Destroying Vulkan Surface.");
  if (context.surface) {
    vkDestroySurfaceKHR(context.instance, context.surface, context.allocator);
    context.surface = 0;
  }
  /* * * * * * * * * */

#ifdef _DEBUG
  M_DEBUG("Destroying Vulkan Debugger.");
  if (context.debug_messenger) {
    PFN_vkDestroyDebugUtilsMessengerEXT func =
        (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(context.instance, 
                                                                   "vkDestroyDebugUtilsMessengerEXT");
    func(context.instance, context.debug_messenger, context.allocator);
  }
#endif

  M_DEBUG("Destroying Vulkan Device.");
  vulkan_device_destroy(&context);

  M_DEBUG("Destroying Vulkan Instance.");
  vkDestroyInstance(context.instance, context.allocator);
}

u0 vulkan_renderer_backend_resized(struct renderer_backend * backend,
                                     const u16 width,
                                     const u16 height) {

  cached_framebuffer_width  = width;
  cached_framebuffer_height = height;
  ++context.framebuffer_size_generation;

  M_DEBUG("%s: w/h/gen: %d/%d/%llu",
          str(vulkan_renderer_backend_resized),
          width,
          height,
          context.framebuffer_size_generation);
}

b8 vulkan_renderer_backend_begin_frame(struct renderer_backend * backend,
                                       const f32 delta_time) {

  vulkan_device * device = &context.device;

  /* check whether
   * the swapchain is being recreated  */
  if (context.recreating_swapchain) {
    VkResult result = vkDeviceWaitIdle(device->logical_device);
    if (!vulkan_result_is_success(result)) {
      M_FATAL("%s %s failed: \'%s\'",
              str(vulkan_renderer_backend_begin_frame),
              str(vkDeviceWaitIdle),
              vulkan_result_string(result, true));
      return false;
    }
    M_DEBUG("Recreating Swapchain..");
    return false;
  }
  /* * * * * * * * * * * * * * * * * * */

  /* check whether
   * the framebuffer has been resized  */
  if (context.framebuffer_size_generation != context.framebuffer_size_last_generation) {
    VkResult result = vkDeviceWaitIdle(device->logical_device);
    if (!vulkan_result_is_success(result)) {
      M_FATAL("%s %s failed: \'%s\'",
              str(vulkan_renderer_backend_begin_frame),
              str(vkDeviceWaitIdle),
              vulkan_result_string(result, true));

      return false;
    }

    if (!recreate_swapchain(backend)) {
      return false;
    }

    /* if the window has been resized, return,
     * since we're not going to be able to render this frame */
    M_DEBUG("Window has been resized.");
    return false;
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  }
  /* * * * * * * * * * * * * * * * * * */

  /* wait for the execution of
   * the current frame to complete */
  if (!vulkan_fence_wait(&context,
                         &context.in_flight_fences[context.current_frame],
                         UINT64_MAX)) {

    M_WARN("Vulkan in-flight fence couldn't wait.");
    return false;
  }
  /* * * * * * * * * * * * * * * * */

  /* acquire the next image from the semaphore,
   * pass along the semaphore that
   * should be signaled when this completes
   * this same semaphore will later be waited on
   * by the queue submission to ensure this image is still available.  */
  if (!vulkan_swapchain_acquire_next_image_index(&context,
                                                 &context.swapchain,
                                                 UINT64_MAX,
                                                 context.image_available_semaphores[context.current_frame],
                                                 0,
                                                 &context.image_index)) {

    return false;
  }
  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  vulkan_command_buffer * command_buffer = &context.graphics_command_buffers[context.image_index];

  /* begin recording commands  */
  vulkan_command_buffer_reset(command_buffer);
  vulkan_command_buffer_begin(command_buffer,
                              false,
                              false,
                              false);
  /* * * * * * * * * * * * * * */

  /* viewport creation */
  VkViewport viewport;
  viewport.x        = 0.0f;
  viewport.y        = (f32)context.framebuffer_height;
  viewport.width    = (f32)context.framebuffer_width;
  viewport.height   = -(f32)context.framebuffer_height;
  viewport.minDepth = 0.0f;
  viewport.maxDepth = 1.0f;
  /* * * * * * * * * * */

  /* scissor creation  */
  VkRect2D scissor;
  scissor.offset.x      = 0;
  scissor.offset.y      = 0;
  scissor.extent.width  = context.framebuffer_width;
  scissor.extent.height = context.framebuffer_height;
  /* * * * * * * * * * */

  /* issue commands  */
  vkCmdSetViewport(command_buffer->handle,
                   0,
                   1,
                   &viewport);
  vkCmdSetScissor(command_buffer->handle,
                  0,
                  1,
                  &scissor);
  /* * * * * * * * * */

  /* copy state  */
  context.main_renderpass.w = context.framebuffer_width;
  context.main_renderpass.h = context.framebuffer_height;
  /* * * * * * * */

  /* begin renderpass  */
  vulkan_renderpass_begin(command_buffer,
                          &context.main_renderpass,
                          context.swapchain.framebuffers[context.image_index].handle);
  /* * * * * * * * * * */

  return true;
}

b8 vulkan_renderer_backend_end_frame(struct renderer_backend * backend,
                                     const f32 delta_time) {

  vulkan_command_buffer * command_buffer = &context.graphics_command_buffers[context.image_index];

  /* end renderpass  */
  vulkan_renderpass_end(command_buffer,
                        &context.main_renderpass);
  /* * * * * * * * * */

  /* end recording */
  vulkan_command_buffer_end(command_buffer);
  /* * * * * * * * */

  /* make sure
   * the previous frame is not using this image  */
  if (context.images_in_flight[context.image_index] != VK_NULL_HANDLE) {
    vulkan_fence_wait(&context,
                      context.images_in_flight[context.image_index],
                      UINT64_MAX);
  }
  /* * * * * * * * * * * * * * * * * * * * * * * */

  /* mark the image fence as
   * in-use by the current frame */
  context.images_in_flight[context.image_index] = &context.in_flight_fences[context.current_frame];
  /* * * * * * * * * * * * * * * */

  /* reset the fence for
   * use in the next frame */
  vulkan_fence_reset(&context,
                     &context.in_flight_fences[context.current_frame]);
  /* * * * * * * * * * * * */

  /* submit the queue and wait
   * for the operation to complete */
  VkSubmitInfo submit_info         = {VK_STRUCTURE_TYPE_SUBMIT_INFO};

  /* command buffers to be executed  */
  submit_info.commandBufferCount   = 1;
  submit_info.pCommandBuffers      = &command_buffer->handle;
  /* * * * * * * * * * * * * * * * * */

  /* semaphores to be signaled
   * when the queue is complete  */
  submit_info.signalSemaphoreCount = 1;
  submit_info.pSignalSemaphores    = &context.queue_complete_semaphores[context.current_frame];
  /* * * * * * * * * * * * * * * */

  /* semaphores to be waited on
   * until the image is available  */
  submit_info.waitSemaphoreCount = 1;
  submit_info.pWaitSemaphores    = &context.image_available_semaphores[context.current_frame];
  /* * * * * * * * * * * * * * * * */

  /* each semaphore waits on
   * the corresponding pipeline stage to complete, 1:1 ratio.
   * VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT prevents
   * subsequent color attachment writes from executing
   * until the semaphore signals
   * (i.e. one frame is presented at a time) */
  VkPipelineStageFlags flags[1] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  submit_info.pWaitDstStageMask = flags;
  /* * * * * * * * * * * * * * * * * * * * * */

  VkResult result = vkQueueSubmit(context.device.graphics_queue,
                                  1,
                                  &submit_info,
                                  context.in_flight_fences[context.current_frame].handle);

  if (VK_SUCCESS != result) {
    M_ERROR("%s failed: \'%s\'",
            str(vkQueueSubmit),
            vulkan_result_string(result, true));
    return false;
  }
  /* * * * * * * * * * * * * * * * */

  vulkan_command_buffer_update_submitted(command_buffer);

  /* present image to the screen */
  vulkan_swapchain_present(&context,
                           &context.swapchain,
                           context.device.graphics_queue,
                           context.device.present_queue,
                           context.queue_complete_semaphores[context.current_frame],
                           context.image_index);
  /* * * * * * * * * * * * * * * */

  return true;
}

VKAPI_ATTR VkBool32 VKAPI_CALL vk_debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
                                                 VkDebugUtilsMessageTypeFlagsEXT message_types,
                                                 const VkDebugUtilsMessengerCallbackDataEXT * data,
                                                 u0 * user_data) {
  switch (message_severity) {
    default:
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
      M_ERROR(data->pMessage);
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
      M_WARN(data->pMessage);
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
      M_INFO(data->pMessage);
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
      M_TRACE(data->pMessage);
      break;
  }
  return VK_FALSE;
}

s32 find_memory_index(u32 type_filter,
                      u32 property_flags) {

  VkPhysicalDeviceMemoryProperties memory_properties;
  vkGetPhysicalDeviceMemoryProperties(context.device.physical_device, &memory_properties);

  for (u32 i = 0; i < memory_properties.memoryTypeCount; ++i) {
    /* check every memory type to verify whether its bit is set to 1 */
    if (type_filter & (1 << i) &&
       (memory_properties.memoryTypes[i].propertyFlags & property_flags) == property_flags) {
      return i;
    }
  }

  M_WARN("Failed finding suitable memory type.");
  return -1;
}

u0 create_command_buffers(renderer_backend * backend) {
  if (!context.graphics_command_buffers) {
    context.graphics_command_buffers = darray_reserve(vulkan_command_buffer, context.swapchain.image_count);
    for (u32 i = 0; i < context.swapchain.image_count; ++i) {
      m_zero_memory(&context.graphics_command_buffers[i], sizeof(vulkan_command_buffer));
    }
  }

  for (u32 i = 0; i < context.swapchain.image_count; ++i) {
    if (context.graphics_command_buffers[i].handle) {
      vulkan_command_buffer_free(&context,
                                 context.device.graphics_command_pool,
                                 &context.graphics_command_buffers[i]);
    }
    m_zero_memory(&context.graphics_command_buffers[i], sizeof(vulkan_command_buffer));
    vulkan_command_buffer_allocate(&context,
                                   context.device.graphics_command_pool,
                                   true,
                                   &context.graphics_command_buffers[i]);
  }
  M_INFO("Successfully created Command Buffers.");
}

u0 regenerate_framebuffers(renderer_backend * backend,
                             vulkan_swapchain * swapchain,
                             vulkan_renderpass * renderpass) {

  for (u32 i = 0; i < swapchain->image_count; ++i) {
    /* TODO: make this dynamic */
    const u32 attachment_count = 2;
    VkImageView attachments[attachment_count] = {
      swapchain->views[i],
      swapchain->depth_attachment.view
    };

    vulkan_framebuffer_create(&context,
                              renderpass,
                              context.framebuffer_width,
                              context.framebuffer_height,
                              attachment_count,
                              attachments,
                              &context.swapchain.framebuffers[i]);
  }
}

b8 recreate_swapchain(renderer_backend * backend) {

  /* check whether
   * the swapchain is already being recreated  */
  if (context.recreating_swapchain) {
    M_DEBUG("%s duplicated call.",
            str(recreate_swapchain));
    return false;
  }
  /* * * * * * * * * * * * * * * * * * * * * * */

  /* check whether
   * the framebuffer is too small  */
  if (context.framebuffer_width == 0 || context.framebuffer_height == 0) {
    M_DEBUG("%s called on small dimensions.",
            str(recreate_swapchain));
    return false;
  }
  /* * * * * * * * * * * * * * * * */

  /* set the flag  */
  context.recreating_swapchain = true;
  /* * * * * * * * */

  /* wait for the device */
  vkDeviceWaitIdle(context.device.logical_device);
  /* * * * * * * * * * * */

  /* reset images  */
  for (u32 i = 0; i < context.swapchain.image_count; ++i) {
    context.images_in_flight[i] = 0;
  }
  /* * * * * * * * */

  /* re-query swapchain support  */
  vulkan_device_query_swapchain_support(context.device.physical_device,
                                        context.surface,
                                        &context.device.swapchain_support);
  /* * * * * * * * * * * * * * * */

  /* re-query depth format */
  vulkan_device_detect_depth_format(&context.device);
  /* * * * * * * * * * * * */

  /* recreate swapchain  */
  vulkan_swapchain_recreate(&context,
                            cached_framebuffer_width,
                            cached_framebuffer_height,
                            &context.swapchain);
  /* * * * * * * * * * * */

  /* sync the framebuffer size */
  context.framebuffer_width                = cached_framebuffer_width;
  context.framebuffer_height               = cached_framebuffer_height;
  context.main_renderpass.w                = context.framebuffer_width;
  context.main_renderpass.h                = context.framebuffer_height;
  cached_framebuffer_width                 = 0;
  cached_framebuffer_height                = 0;
  context.framebuffer_size_last_generation = context.framebuffer_size_generation;
  /* * * * * * * * * * * * * * */

  /* XXX: join both for loops? */

  /* cleanup swapchain */
  for (u32 i = 0; i < context.swapchain.image_count; ++i) {
    vulkan_command_buffer_free(&context,
                               context.device.graphics_command_pool,
                               &context.graphics_command_buffers[i]);
  }
  /* * * * * * * * * * */

  /* cleanup framebuffers  */
  for (u32 i = 0; i < context.swapchain.image_count; ++i) {
    vulkan_framebuffer_destroy(&context,
                               &context.swapchain.framebuffers[i]);
  }
  /* * * * * * * * * * * * */

  /* create render-area  */
  context.main_renderpass.x = 0;
  context.main_renderpass.y = 0;
  context.main_renderpass.w = context.framebuffer_width;
  context.main_renderpass.h = context.framebuffer_height;
  /* * * * * * * * * * * */

  /* re-generate framebuffers  */
  regenerate_framebuffers(backend,
                          &context.swapchain,
                          &context.main_renderpass);
  create_command_buffers(backend);
  /* * * * * * * * * * * * * * */

  /* unset the flag  */
  context.recreating_swapchain = false;
  /* * * * * * * * * */

  return true;
}
