#pragma once

#include "defines.h"
#include "renderer/renderer_backend.h"

b8 vulkan_renderer_backend_initialize(struct renderer_backend * backend, const char * const title, struct platform_state * plat_state);
u0 vulkan_renderer_backend_shutdown(struct renderer_backend * backend);
u0 vulkan_renderer_backend_resized(struct renderer_backend * backend, const u16 width, const u16 height);
b8 vulkan_renderer_backend_begin_frame(struct renderer_backend * backend, const f32 delta_time);
b8 vulkan_renderer_backend_end_frame(struct renderer_backend * backend, const f32 delta_time);
