#pragma once

#include "defines.h"
#include "vulkan_types.h"

u0 vulkan_renderpass_create(vulkan_context * context,
                              vulkan_renderpass * out_renderpass,
                              f32 x, f32 y, f32 w, f32 h, /* source coordinates */
                              f32 r, f32 g, f32 b, f32 a, /* clearcolor */
                              f32 depth,
                              u32 stencil);

u0 vulkan_renderpass_destroy(vulkan_context * context,
                               vulkan_renderpass * renderpass);

u0 vulkan_renderpass_begin(vulkan_command_buffer * command_buffer,
                             vulkan_renderpass * renderpass,
                             VkFramebuffer framebuffer);

u0 vulkan_renderpass_end(vulkan_command_buffer * command_buffer, vulkan_renderpass * renderpass);
