#pragma once

#include <defines.h>
#include "vulkan_types.h"

u0 vulkan_framebuffer_create(vulkan_context * context,
                               vulkan_renderpass * renderpass,
                               u32 width,
                               u32 height,
                               u32 attachment_count,
                               VkImageView * attachments,
                               vulkan_framebuffer * out_framebuffer);

u0 vulkan_framebuffer_destroy(vulkan_context * context,
                                vulkan_framebuffer * framebuffer);
