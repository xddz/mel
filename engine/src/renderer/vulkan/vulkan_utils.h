#pragma once

#include <defines.h>
#include "vulkan_types.h"

/* return the string representation of `result`  */
const char * vulkan_result_string(const VkResult result,
                                  const b8 get_extended);
/* * * * * * * * * * * * * * * * * * * * * * * * */

/* indicate if `result` is successful
 * as indicated by the Vulkan Specification  */
b8 vulkan_result_is_success(const VkResult result);
/* * * * * * * * * * * * * * * * * * * * * * */
