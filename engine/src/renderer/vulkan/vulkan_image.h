#pragma once

#include "defines.h"
#include "vulkan_types.h"

u0 vulkan_image_create(vulkan_context * context,
                         VkImageType image_type,
                         u32 width,
                         u32 height,
                         VkFormat format,
                         VkImageTiling tiling,
                         VkImageUsageFlags usage,
                         VkMemoryPropertyFlags memory_flags,
                         b8 create_view,
                         VkImageAspectFlags view_aspect_flags,
                         vulkan_image * out_image);

u0 vulkan_image_view_create(vulkan_context * context,
                              VkFormat format,
                              vulkan_image * image,
                              VkImageAspectFlags aspect_flags);

u0 vulkan_image_destroy(vulkan_context * context, vulkan_image * image);
