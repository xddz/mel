#pragma once

#include "defines.h"

struct platform_state;
struct vulkan_context;

b8 platform_create_vulkan_surface(struct platform_state * plat_state, struct vulkan_context * context);

/* appends a string with the required 
 * platform-specific extensions to the provided darray */
u0 platform_get_required_extension_names(const char *** ext_darray);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * */
