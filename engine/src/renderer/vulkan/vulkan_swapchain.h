#pragma once

#include "defines.h"
#include "vulkan_types.h"

u0 vulkan_swapchain_create(vulkan_context * context,
                             u16 width, u16 height,
                             vulkan_swapchain * out_swapchain);

u0 vulkan_swapchain_recreate(vulkan_context * context,
                               u16 width, u16 height,
                               vulkan_swapchain * swapchain);

u0 vulkan_swapchain_destroy(vulkan_context * context,
                              vulkan_swapchain * swapchain);

b8 vulkan_swapchain_acquire_next_image_index(vulkan_context * context,
                                             vulkan_swapchain * swapchain,
                                             u64 timeout_ns,
                                             VkSemaphore image_available_semaphore,
                                             VkFence fence,
                                             u32 * out_image_index);

u0 vulkan_swapchain_present(vulkan_context * context,
                              vulkan_swapchain * swapchain,
                              VkQueue graphics_queue,
                              VkQueue present_queue,
                              VkSemaphore render_complete_semaphore,
                              u32 present_image_index);
