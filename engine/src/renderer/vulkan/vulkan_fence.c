#include "vulkan_fence.h"

#include <core/logger.h>

u0 vulkan_fence_create(vulkan_context * context,
                         const b8 create_signaled,
                         vulkan_fence * out_fence) {

  out_fence->is_signaled = create_signaled;
  VkFenceCreateInfo fence_create_info = {VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
  if (out_fence->is_signaled) {
    fence_create_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  }

  VK_CHECK(vkCreateFence(context->device.logical_device,
                         &fence_create_info,
                         context->allocator,
                         &out_fence->handle));
}

u0 vulkan_fence_destroy(vulkan_context * context,
                          vulkan_fence * fence) {

  if (fence->handle) {
    vkDestroyFence(context->device.logical_device,
                   fence->handle,
                   context->allocator);
    fence->handle = 0;
  }
  fence->is_signaled = false;
}

b8 vulkan_fence_wait(vulkan_context * context,
                     vulkan_fence * fence,
                     const u64 timeout_ns) {

  if (fence->is_signaled) {
    return true;
  }

  VkResult result = vkWaitForFences(context->device.logical_device,
                                    1,
                                    &fence->handle,
                                    true,
                                    timeout_ns);

  switch (result) {
    default: {
      M_ERROR("%s - An unknown error has occurred.", str(vulkan_fence_wait));
      break;
    }
    case VK_SUCCESS: {
      fence->is_signaled = true;
      return true;
    }
    case VK_TIMEOUT: {
      M_ERROR("%s - %s.", str(vulkan_fence_wait), str(VK_TIMEOUT));
      break;
    }
    case VK_ERROR_DEVICE_LOST: {
      M_ERROR("%s - %s.", str(vulkan_fence_wait), str(VK_ERROR_DEVICE_LOST));
      break;
    }
    case VK_ERROR_OUT_OF_HOST_MEMORY: {
      M_ERROR("%s - %s.", str(vulkan_fence_wait), str(VK_ERROR_OUT_OF_HOST_MEMORY));
      break;
    }
    case VK_ERROR_OUT_OF_DEVICE_MEMORY: {
      M_ERROR("%s - %s.", str(vulkan_fence_wait), str(VK_ERROR_OUT_OF_DEVICE_MEMORY));
      break;
    }
  }
  return false;
}

u0 vulkan_fence_reset(vulkan_context * context,
                        vulkan_fence * fence) {

  if (fence->is_signaled) {
    VK_CHECK(vkResetFences(context->device.logical_device,
                           1,
                           &fence->handle));
    fence->is_signaled = false;
  }
}
