#pragma once

#include "defines.h"
#include "renderer_types.h"

struct static_mesh_data;
struct platform_state;

b8 renderer_initialize(const char * const title, struct platform_state * platform);
u0 renderer_shutdown(u0);

u0 renderer_on_resized(const u16 width, const u16 height);

b8 renderer_draw_frame(render_packet * packet);