#pragma once

#include "defines.h"

typedef enum renderer_backend_type {
  RENDERER_BACKEND_TYPE_VULKAN,
  RENDERER_BACKEND_TYPE_OPENGL,
  RENDERER_BACKEND_TYPE_DIRECTX,
} renderer_backend_type;

typedef struct renderer_backend {
  struct platform_state * platform;
  u64 frame_number;

  b8 (*initialize)(struct renderer_backend * backend, const char * const title, struct platform_state * platform);
  u0 (*shutdown)(struct renderer_backend * backend);
  u0 (*resized)(struct renderer_backend * backend, const u16 width, const u16 height);
  b8 (*begin_frame)(struct renderer_backend * backend, const f32 delta_time);
  b8 (*end_frame)(struct renderer_backend * backend, const f32 delta_time);
} renderer_backend;

typedef struct render_packet {
  f32 delta_time;
} render_packet;