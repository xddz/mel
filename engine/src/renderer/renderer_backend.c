#include "renderer_backend.h"

#include "vulkan/vulkan_backend.h"

b8 renderer_backend_create(const renderer_backend_type type,
                           struct platform_state * platform,
                           renderer_backend * backend) {

  backend->platform = platform;

  switch (type) {
    default:
      return false;
    case RENDERER_BACKEND_TYPE_VULKAN: {
      /* TODO: fill out */
      backend->initialize  = vulkan_renderer_backend_initialize;
      backend->shutdown    = vulkan_renderer_backend_shutdown;
      backend->begin_frame = vulkan_renderer_backend_begin_frame;
      backend->end_frame   = vulkan_renderer_backend_end_frame;
      backend->resized     = vulkan_renderer_backend_resized;
      break;
    }
  }
  return true;
}

u0 renderer_backend_destroy(renderer_backend * backend) {
  backend->initialize  = 0;
  backend->shutdown    = 0;
  backend->begin_frame = 0;
  backend->end_frame   = 0;
  backend->resized     = 0;
}
