#pragma once

#include "defines.h"
#include "renderer_types.h"

struct platform_state;

b8 renderer_backend_create(const renderer_backend_type type, struct platform_state * platform, renderer_backend * backend);
u0 renderer_backend_destroy(renderer_backend * backend);