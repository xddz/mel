#include "./input.h"
#include "./event.h"
#include "./m_memory.h"
#include "./logger.h"

typedef struct keyboard_state {
  b8 keys[256];
} keyboard_state;

typedef struct mouse_state {
  s16 x, y;
  u8 buttons[BUTTON_MAX_BUTTONS];
} mouse_state;

typedef struct input_state {
  keyboard_state keyboard_current, keyboard_previous;
  mouse_state mouse_current, mouse_previous;
} input_state;

/* internal input state  */
static b8 initialized = false;
static input_state state = {};
/* * * * * * * * * * * * */

u0 input_initialize(u0) {
  m_zero_memory(&state, sizeof(input_state));
  initialized = true;
  M_INFO("Input subsystem initialized.");
}

u0 input_shutdown(u0) {
  /* TODO: add shutdown routines */
  initialized = false;
}

u0 input_update(f64 delta_time) {
  if (!initialized) {
    return;
  }

  /* copy "current" states to "previous" states  */
  m_copy_memory(&state.keyboard_previous, &state.keyboard_current, sizeof(keyboard_state));
  m_copy_memory(&state.mouse_previous, &state.mouse_current, sizeof(mouse_state));
  /* * * * * * * * * * * * * * * * * * * * * * * */
}

u0 input_process_key(keys key,
                       b8 pressed) {

  /* verify whether the state changed or not  */
  if (state.keyboard_current.keys[key] != pressed) {
    /* update internal state */
    state.keyboard_current.keys[key] = pressed;
    /* * * * * * * * * * * * */

    /* fire event  */
    event_context context;
    context.data.u16[0] = key;
    event_fire(pressed ? EVENT_CODE_KEY_PRESSED : EVENT_CODE_KEY_RELEASED, 0, context);
    /* * * * * * * */
  }
  /* * * * * * * * * * * * * *  * * * * * * * */
}

u0 input_process_button(buttons button,
                          b8 pressed) {

  /* verify whether the state changed or not  */
  if (state.mouse_current.buttons[button] != pressed) {
    /* update internal state */
    state.mouse_current.buttons[button] = pressed;
    /* * * * * * * * * * * * */

    /* fire event  */
    event_context context;
    context.data.u16[0] = button;
    event_fire(pressed ? EVENT_CODE_BUTTON_PRESSED : EVENT_CODE_BUTTON_RELEASED, 0, context);
    /* * * * * * * */
  }
  /* * * * * * * * * * * * * *  * * * * * * * */
}

u0 input_process_mouse_move(s16 x,
                              s16 y) {
  /* verify whether the state changed or not  */
  if (state.mouse_current.x != x || state.mouse_current.y != y) {
    
    M_DEBUG("mouse position: [%dx%d]", x, y);
    
    /* update internal state */
    state.mouse_current.x = x;
    state.mouse_current.y = y;
    /* * * * * * * * * * * * */

    /* fire event  */
    event_context context;
    context.data.u16[0] = x;
    context.data.u16[1] = y;
    event_fire(EVENT_CODE_MOUSE_MOVED, 0, context);
    /* * * * * * * */
  }
  /* * * * * * * * * * * * * *  * * * * * * * */
}

u0 input_process_mouse_wheel(s8 z_delta) {
  /* fire event  */
  event_context context;
  context.data.u8[0] = z_delta;
  event_fire(EVENT_CODE_MOUSE_WHEEL, 0, context);
  /* * * * * * * */
}

b8 input_is_key_down(keys key) {
  if (!initialized) {
    return false;
  }
  return state.keyboard_current.keys[key] == true;
}

b8 input_is_key_up(keys key) {
  if (!initialized) {
    return true;
  }
  return state.keyboard_current.keys[key] == false;
}

b8 input_was_key_down(keys key) {
  if (!initialized) {
    return false;
  }
  return state.keyboard_previous.keys[key] == true;
}

b8 input_was_key_up(keys key) {
  if (!initialized) {
    return true;
  }
  return state.keyboard_previous.keys[key] == false;
}

b8 input_is_button_down(buttons button) {
  if (!initialized) {
    return false;
  }
  return state.mouse_current.buttons[button] == true;
}

b8 input_is_button_up(buttons button) {
  if (!initialized) {
    return true;
  }
  return state.mouse_current.buttons[button] == false;
}

b8 input_was_button_down(buttons button) {
  if (!initialized) {
    return false;
  }
  return state.mouse_previous.buttons[button] == true;
}

b8 input_was_button_up(buttons button) {
  if (!initialized) {
    return true;
  }
  return state.mouse_previous.buttons[button] == false;
}


u0 input_get_mouse_position(s16 * x,
                              s16 * y) {

  if (!initialized) {
    *x = 0;
    *y = 0;
    return;
  }
  *x = state.mouse_current.x;
  *y = state.mouse_current.y;
}

u0 input_get_previous_mouse_position(s16 * x,
                                       s16 * y) {

  if (!initialized) {
    *x = 0;
    *y = 0;
    return;
  }

  *x = state.mouse_previous.x;
  *y = state.mouse_previous.y;
}
