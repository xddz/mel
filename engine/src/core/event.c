#include "./event.h"
#include "./m_memory.h"
#include "../containers/darray.h"

typedef struct registered_event {
  u0 * listener;
  PFN_on_event callback;
} registered_event;

typedef struct event_code_entry {
  registered_event * events;
} event_code_entry;

/* more than enough codes */
#define MAX_MESSAGE_CODES (16384)

/* state structure */
typedef struct event_system_state {
  /* lookup table for event codes */
  event_code_entry registered[MAX_MESSAGE_CODES];
} event_system_state;
/* * * * * * * * * */

/* event system internal state */
static b8 is_initialized = false;
static event_system_state state;
/* * * * * * * * * * * * * * * */

b8 event_initialize(u0) {
  if (is_initialized == true) {
    return false;
  }
  m_zero_memory(&state, sizeof(state));
  is_initialized = true;
  return true;
}

u0 event_shutdown(u0) {
  /* free event arrays */
  for (u16 i = 0; i < MAX_MESSAGE_CODES; ++i) {
    if (state.registered[i].events != 0) {
      darray_destroy(state.registered[i].events);
      state.registered[i].events = 0;
    }
  }
  /* * * * * * * * * * */
}

b8 event_register(const u16 code,
                  u0 * listener,
                  PFN_on_event on_event) {

  if (is_initialized == false) {
    return false;
  }

  /* only create event array once  */
  if (state.registered[code].events == 0) {
    state.registered[code].events = darray_create(registered_event);
  }
  /* * * * * * * * * * * * * * * * */

  u64 registered_count = darray_length(state.registered[code].events);
  for (u64 i = 0; i < registered_count; ++i) {
    if (state.registered[code].events[i].listener == listener) {
      /* TODO: warning */
      return false;
    }
  }

  /* no duplicate has been found */
  registered_event event;
  event.listener = listener;
  event.callback = on_event;
  darray_push(state.registered[code].events, event);
  return true;
}

b8 event_unregister(const u16 code,
                    u0 * listener,
                    PFN_on_event on_event) {

  if (is_initialized == false) {
    return false;
  }

  /* nothing has been registered */
  if (state.registered[code].events == 0) {
    /* TODO: warning */
    return false;
  }
  /* * * * * * * * * * * * * * * */

  u64 registered_count = darray_length(state.registered[code].events);
  for (u64 i = 0; i < registered_count; ++i) {
    registered_event e = state.registered[code].events[i];
    if (e.listener == listener && e.callback == on_event) {
      /* found event */
      registered_event popped_event;
      darray_pop_at(state.registered[code].events, i, &popped_event);
      return true;
    }
  }

  /* event not found */
  return false;
}

b8 event_fire(const u16 code,
              u0 * sender,
              event_context context) {

  if (is_initialized == false) {
    return false;
  }

  /* nothing has been registered */
  if (state.registered[code].events == 0) {
    /* TODO: warning */
    return false;
  }
  /* * * * * * * * * * * * * * * */

  u64 registered_count = darray_length(state.registered[code].events);
  for (u64 i = 0; i < registered_count; ++i) {
    registered_event e = state.registered[code].events[i];
    if (e.callback(code, sender, e.listener, context)) {
      /* message has been handled */
      return true;
    }
  }

  /* event not found */
  return false;
}
