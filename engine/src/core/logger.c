#include "./logger.h"
#include "./asserts.h"
#include "../platform/platform.h"

/* TODO: temporary */
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

b8 initialize_logging(u0) {
  /* TODO: create log file */
  return true;
}

u0 shutdown_logging(u0) {
  /* TODO: shutdown logging/write queued entries */
  return;
}

/* 
   "[/]: fatal-level   log output" 
   "[-]: error-level   log output"
   "[!]: warning-level log output"
   "[#]: info-level    log output"
   "[~]: debug-level   log output"
   "[>]: trace-level   log output"
*/
u0 log_output(log_level level,
                const char * const message,
                ...) {

  const char * const level_repr = "/-!#~>";
  const b8 is_error = (level < LOG_LEVEL_WARN);

  /* NOTE: this is a workaround 
     for overwritten va_list on windows */
  __builtin_va_list args; 
  va_start(args, message);

  char msg[2048] = "\0";
  vsnprintf(msg, 2048, message, args);

  char output[2048] = "\0";
  snprintf(output, 2048, "[%c]: %s", level_repr[level], msg);

  /* platform-specific output */
  if (is_error) {
    platform_console_write_error(output, level);
  }else{
    platform_console_write(output, level);
  }

  va_end(args);
  return;
}
/* * * * * * * * * * * * * * * * */

/* asserts.h */
u0 report_assertion_failure(const char * const expression,
                              const char * const message,
                              const char * const file,
                              s32 line) {

  log_output(LOG_LEVEL_FATAL, "Assertion Failure: %s, message: \"%s\", in file: %s, line %d\n", expression, message, file, line);
}
/* * * * * * */

