#include "./m_memory.h"
#include "./logger.h"
#include "../platform/platform.h"

#include "./m_string.h"

struct memory_stats {
  u64 total_allocated;
  u64 tagged_allocations[MEMORY_TAG_MAX_TAGS];
};

static struct memory_stats stats;

static const char * memory_tag_strings[] = {
  str(MEMORY_TAG_UNKNOWN), /* TODO: temporary */
  str(MEMORY_TAG_ARRAY),
  str(MEMORY_TAG_DARRAY),
  str(MEMORY_TAG_DICT),
  str(MEMORY_TAG_RING_QUEUE),
  str(MEMORY_TAG_BST),
  str(MEMORY_TAG_STRING),
  str(MEMORY_TAG_APPLICATION),
  str(MEMORY_TAG_JOB),
  str(MEMORY_TAG_TEXTURE),
  str(MEMORY_TAG_MATERIAL_INSTANCE),
  str(MEMORY_TAG_RENDERER),
  str(MEMORY_TAG_GAME),
  str(MEMORY_TAG_TRANSFORM),
  str(MEMORY_TAG_ENTITY),
  str(MEMORY_TAG_ENTITY_NODE),
  str(MEMORY_TAG_SCENE),

  str(MEMORY_TAG_MAX_TAGS)
};

u0 initialize_memory(u0) {
  platform_zero_memory(&stats, sizeof(stats));
}

u0 shutdown_memory(u0) {
}

u0 * m_allocate(const u64 size,
                  const memory_tag tag) {

  if (tag == MEMORY_TAG_UNKNOWN) {
    M_WARN("%s(%d, %s);", __FUNCTION__, size, str(MEMORY_TAG_UNKNOWN));
  }

  stats.total_allocated += size;
  stats.tagged_allocations[tag] += size;

  /* TODO: memory alignment */
  u0 * block = platform_allocate(size, false);
  platform_zero_memory(block, size);

  return block;
}

u0 m_free(u0 * block,
            const u64 size,
            const memory_tag tag) {

  if (tag == MEMORY_TAG_UNKNOWN) {
    M_WARN("%s(%d, %s);", __FUNCTION__, size, str(MEMORY_TAG_UNKNOWN));
  }

  stats.total_allocated -= size;
  stats.tagged_allocations[tag] -= size;

  /* TODO: memory alignment */
  platform_free(block, false);
}

u0 * m_zero_memory(u0 * block,
                     const u64 size) {

  return platform_zero_memory(block, size);
}

u0 * m_copy_memory(u0 * dest,
                     const u0 * source,
                     const u64 size) {

  return platform_copy_memory(dest, source, size);
}

u0 * m_set_memory(u0 * dest,
                    const s32 value,
                    const u64 size) {

  return platform_set_memory(dest, value, size);
}

u0 print_memory_usage(u0) {
  const u64 gib = 1024 * 1024 * 1024;
  const u64 mib = 1024 * 1024;
  const u64 kib = 1024;

  M_DEBUG("System memory use (tagged):");
  for (u32 i = 0; i < MEMORY_TAG_MAX_TAGS; ++i) {
    const u64    unit[]     = {1   , kib   , mib   , gib};
    const char * unit_str[] = {"B" , "KiB" , "MiB" , "GiB"};
    u32 p = 0;
    for (; p < (sizeof(unit_str)/sizeof(unit_str[0])); ++p) {
      if ((unit[p] - stats.tagged_allocations[i]) > 0) {
        break;
      }
    }
    f32 amount = stats.tagged_allocations[i] / (f32)unit[p];

    M_DEBUG("  %s: %*.2f%s", memory_tag_strings[i], (int)(40 - string_length(memory_tag_strings[i])), amount, unit_str[p]);
  }
}
