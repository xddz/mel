#pragma once

#include "../defines.h"

#define M_ASSERTIONS_ENABLED

#ifdef M_ASSERTIONS_ENABLED
#if _MSC_VER
#include <intrin.h>
#define debug_break() __debugbreak()
#else
#define debug_break() __builtin_trap()
#endif

M_API u0 report_assertion_failure(const char * const expression, const char * const message, const char * const file, s32 line);

#define M_ASSERT(expr){                                              \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, "", __FILE__, __LINE__);       \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}

#define M_ASSERT_MSG(expr, message){                                 \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, message, __FILE__, __LINE__);  \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}

#ifdef _DEBUG
#define M_ASSERT_DEBUG(expr){                                        \
  do{                                                                \
    if (!(expr)){                                                    \
      report_assertion_failure(#expr, "", __FILE__, __LINE__);       \
      debug_break();                                                 \
    }                                                                \
  }while(0);                                                         \
}
#else
#define M_ASSERT_DEBUG(expr)
#endif

#else
#define M_ASSERT(expr)
#define M_ASSERT_MSG(expr, message)
#define M_ASSERT_DEBUG(expr)
#endif
