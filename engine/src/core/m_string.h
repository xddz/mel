#pragma once

#include "defines.h"

M_API const u64 string_length(const char * str);
M_API char * string_duplicate(const char * str);

/* case sensitive comparison */
M_API b8 strings_equal(const char * const s1,
                       const char * const s2);
