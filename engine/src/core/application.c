#include "./application.h"
#include "../mel.h"
#include "./logger.h"
#include "../platform/platform.h"
#include "../core/m_memory.h"
#include "./event.h"
#include "./input.h"
#include "core/clock.h"
#include "renderer/renderer_frontend.h"

typedef struct application_state {
  mel_t * ctx;
  b8 is_running;
  b8 is_suspended;
  platform_state platform;
  s16 width, height;
  clock clock;
  f64 last_time;
} application_state;

static b8 initialized = false;
static application_state app_state;

/* event handlers  */
b8 application_on_event(const u16 code,
                        u0 * sender,
                        u0 * listener_inst,
                        event_context context);

b8 application_on_key(const u16 code,
                      u0 * sender,
                      u0 * listener_inst,
                      event_context context);

b8 application_on_resized(const u16 code,
                          u0 * sender,
                          u0 * listener_inst,
                          event_context context);
/* * * * * * * * * */

b8 application_create(mel_t * ctx) {
  if (initialized) {
    M_ERROR("application_create called more than once.");
    return false;
  }

  app_state.ctx = ctx;

  initialize_logging();
  input_initialize();

  // TODO: remove all this
  M_FATAL("This is a fatal-level log message: %.2f", 3.14159f);
  M_ERROR("This is an error-level log message: %.2f", 3.14159f);
  M_WARN("This is a warning-level log message: %.2f", 3.14159f);
  M_INFO("This is an info-level log message: %.2f", 3.14159f);
  M_DEBUG("This is a debug-level log message: %.2f", 3.14159f);
  M_TRACE("This is a trace-level log message: %.2f", 3.14159f);

  app_state.is_running = true;
  app_state.is_suspended = false;

  if (!event_initialize()) {
    M_FATAL("Failed initializing event system.");
    return false;
  }

  event_register(EVENT_CODE_APPLICATION_QUIT, 0, application_on_event);
  event_register(EVENT_CODE_KEY_PRESSED, 0, application_on_key);
  event_register(EVENT_CODE_KEY_RELEASED, 0, application_on_key);
  event_register(EVENT_CODE_RESIZED, 0, application_on_resized);

  if (!platform_startup(&app_state.platform       , ctx->app_config.name, 
                       ctx->app_config.start_pos_x, ctx->app_config.start_pos_y, 
                       ctx->app_config.start_width, ctx->app_config.start_height)) {
    return false;
  }

  /* initialize the renderer */
  if (!renderer_initialize(ctx->app_config.name, &app_state.platform)) {
    M_FATAL("Failed initializing renderer.");
    return false;
  }
  /* * * * * * * * * * * * * */

  /* initialize the application  */
  if (!app_state.ctx->initialize(app_state.ctx)) {
    M_FATAL("Failed initializing application");
    return false;
  }
  /* * * * * * * * * * * * * * * */

  app_state.ctx->on_resize(app_state.ctx, app_state.width, app_state.height);

  initialized = true;
  return true;
}

b8 application_run(u0) {
  clock_start(&app_state.clock);

  /* update clock  */
  clock_update(&app_state.clock);
  app_state.last_time = app_state.clock.elapsed;
  f64 running_time = 0;
  u8 frame_count = 0;
  f64 target_frame_seconds = 1.0f / 60; /* target frame rate is set to 60 */
  /* * * * * * * * */

  print_memory_usage();

  while (app_state.is_running) {
    if (!platform_pump_messages(&app_state.platform)) {
      app_state.is_running = false;
    }

    if (!app_state.is_suspended) {
      /* update clock  */
      clock_update(&app_state.clock);
      f64 current_time = app_state.clock.elapsed;
      f64 delta = (current_time - app_state.last_time);
      f64 frame_start_time = platform_get_absolute_time();
      /* * * * * * * * */

      if (!app_state.ctx->update(app_state.ctx, (f32)delta)) {
        M_FATAL("Failed updating.");
        app_state.is_running = false;
        break;
      }

      if (!app_state.ctx->render(app_state.ctx, (f32)delta)) {
        M_FATAL("Failed rendering.");
        app_state.is_running = false;
        break;
      }

      /* TODO: change this */
      render_packet packet;
      packet.delta_time = delta;
      renderer_draw_frame(&packet);

      /* apply vsync */
      f64 frame_end_time = platform_get_absolute_time();
      f64 frame_elapsed_time = frame_end_time - frame_start_time;
      running_time += frame_elapsed_time;
      f64 remaining_seconds = target_frame_seconds - frame_elapsed_time;
      
      if (remaining_seconds > 0) {
        u64 remaining_ms = (remaining_seconds * 1000);
        b8 vsync = false;
        
        if (remaining_ms > 0 && vsync) {
          platform_sleep(remaining_ms - 1);
        }
        ++frame_count;
      }
      /* * * * * * * */

      input_update(delta);
      app_state.last_time = current_time;
    }
  }
  app_state.is_running = false;

  event_unregister(EVENT_CODE_APPLICATION_QUIT, 0, application_on_event);
  event_unregister(EVENT_CODE_KEY_PRESSED, 0, application_on_key);
  event_unregister(EVENT_CODE_KEY_RELEASED, 0, application_on_key);

  event_shutdown();
  input_shutdown();

  renderer_shutdown();
  platform_shutdown(&app_state.platform);

  return true;
}

u0 application_get_framebuffer_size(u16 * width,
                                      u16 * height) {

  *width = app_state.width;
  *height = app_state.height;
}

b8 application_on_event(const u16 code,
                        u0 * sender,
                        u0 * listener_inst,
                        event_context context) {

  switch (code) {
    case EVENT_CODE_APPLICATION_QUIT: {
      M_INFO("Received [%s] signal, shutting down.", str(EVENT_CODE_APPLICATION_QUIT));
      app_state.is_running = false;
      return true;
    }
  }
  
  return false;
}

b8 application_on_key(const u16 code,
                      u0 * sender,
                      u0 * listener_inst,
                      event_context context) {

  u16 key_code = context.data.u16[0];
  switch (code) {
    case EVENT_CODE_KEY_PRESSED: {
      if (key_code == KEY_ESCAPE) {
        event_context data = {};
        event_fire(EVENT_CODE_APPLICATION_QUIT, 0, data);
        return true;
      } else {
        M_DEBUG("\'%c\' has been pressed.", key_code);
      }
      break;
    }
    
    case EVENT_CODE_KEY_RELEASED: {
      M_DEBUG("\'%c\' has been released.", key_code);
      break;
    }
  }
  return false;
}

b8 application_on_resized(const u16 code,
                          u0 * sender,
                          u0 * listener_inst,
                          event_context context) {

  if (code != EVENT_CODE_RESIZED) {
    return false;
  }

  u16 width  = context.data.u16[0];
  u16 height = context.data.u16[1];
  if (width != app_state.width || height != app_state.height) {
    app_state.width  = width;
    app_state.height = height;

    M_DEBUG("Window resize: %dx%d", width, height);

    if (!width || !height) {
      M_DEBUG("Window has been minimized.");
      app_state.is_suspended = true;
      return true;
    }

    if (app_state.is_suspended) {
      M_DEBUG("Window restored.");
      app_state.is_suspended = false;
    }

    app_state.ctx->on_resize(app_state.ctx, width, height);
    renderer_on_resized(width, height);
  }

  /* event unhandled on purpose  */
  return false;
  /* * * * * * * * * * * * * * * */
}
