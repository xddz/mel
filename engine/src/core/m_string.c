#include "./m_string.h"
#include "./m_memory.h"
#include <string.h>

const u64 string_length(const char * str) {
  return strlen(str);
}

char * string_duplicate(const char * str) {
  const u64 length = string_length(str);
  char * copy = m_allocate(length + 1, MEMORY_TAG_STRING);
  m_copy_memory(copy, str, length + 1);
  return copy;
}

/* case sensitive comparison */
b8 strings_equal(const char * const s1,
                 const char * const s2) {

  return (strcmp(s1, s2) == 0);
}
