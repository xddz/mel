#pragma once

#include "../defines.h"

struct mel_t;

/* application configuration */
typedef struct application_config {
  s16 start_pos_x, start_pos_y;
  s16 start_width, start_height;
  char * name;
} application_config;
/* * * * * * * * * * * * * * */

M_API b8 application_create(struct mel_t * ctx);
M_API b8 application_run(u0);

u0 application_get_framebuffer_size(u16 * width,
                                      u16 * height);
