#pragma once

#include "defines.h"

typedef struct clock {
  f64 start_time;
  f64 elapsed;
} clock;

/* updates the provided clocks elapsed time  */
u0 clock_update(clock * clock);
/* * * * * * * * * * * * * * * * * * * * * * */

/* starts the provided clock */
u0 clock_start(clock * clock);
/* * * * * * * * * * * * * * */

/* stops the provided clock  */
u0 clock_stop(clock * clock);
/* * * * * * * * * * * * * * */