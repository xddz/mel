#pragma once

#include <defines.h>

typedef struct event_context {
  /* maximum of 128 bytes */
  union {
    s64 s64[2];
    u64 u64[2];
    f64 f64[2];

    s32 s32[4];
    u32 u32[4];
    f32 f32[4];

    s16 s16[8];
    u16 u16[8];

    s8 s8[16];
    u8 u8[16];
    
    char c[16];
  } data;
} event_context;

typedef b8 (*PFN_on_event)(const u16 code, u0 * sender, u0 * listener_inst, const event_context data);

b8 event_initialize(u0);
u0 event_shutdown(u0);

/* register to listen for when events are sent
 * with the provided code. events with duplicate listener/callback combos
 * will not be registered again and will cause the function to return false  */
M_API b8 event_register(const u16 code,
                        u0 * listener,
                        PFN_on_event on_event);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* unregister from listening for 
 * when events are sent with the provided code.
 * if no matching registration is found the function will return false */
M_API b8 event_unregister(const u16 code,
                          u0 * listener,
                          PFN_on_event on_event);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* fires an event to listeners of the provided code.
 * if an event handler returns true, 
 * the event is considered handled and is not passed on to any more listeners  */
M_API b8 event_fire(const u16 code,
                    u0 * sender,
                    event_context context);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* system internal event codes */
typedef enum system_event_code {
  /* shutdown the application on the next frame  */
  EVENT_CODE_APPLICATION_QUIT = 0x01,
  /* * * * * * * * * * * * * * * * * * * * * * * */

  /* keyboard key has been pressed:
   * u16 key_code = data.data.u16[0];  */
  EVENT_CODE_KEY_PRESSED      = 0x02,
  /* * * * * * * * * * * * * * * * * * */

  /* keyboard key has been released:
   * u16 key_code = data.data.u16[0];  */
  EVENT_CODE_KEY_RELEASED     = 0x03,
  /* * * * * * * * * * * * * * * * * * */

  /* mouse button has been pressed:
   * u16 button   = data.data.u16[0];  */
  EVENT_CODE_BUTTON_PRESSED   = 0x04,
  /* * * * * * * * * * * * * * * * * * */

  /* mouse button has been released:
   * u16 button   = data.data.u16[0];  */
  EVENT_CODE_BUTTON_RELEASED  = 0x05,
  /* * * * * * * * * * * * * * * * * * */

  /* mouse coordinates have changed:
   * u16 x        = data.data.u16[0];
   * u16 y        = data.data.u16[1];  */
  EVENT_CODE_MOUSE_MOVED      = 0x06,
  /* * * * * * * * * * * * * * * * * * */

  /* mouse wheel coordinates have changed:
   * u8  z_delta  = data.data.u8[0];   */
  EVENT_CODE_MOUSE_WHEEL      = 0x07,
  /* * * * * * * * * * * * * * * * * * */

  /* window size has changed:
   * u16 width    = data.data.u16[0];
   * u16 height   = data.data.u16[1];  */
  EVENT_CODE_RESIZED          = 0x08,
  /* * * * * * * * * * * * * * * * * * */

  MAX_EVENT_CODE = 0xFF
} system_event_code;
/* * * * * * * * * * * * * * * */
