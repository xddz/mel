#include "clock.h"
#include "platform/platform.h"

/* updates the provided clocks elapsed time  */
u0 clock_update(clock * clock) {
  if (clock->start_time != 0) {
    clock->elapsed = platform_get_absolute_time() - clock->start_time;
  }
}
/* * * * * * * * * * * * * * * * * * * * * * */

/* starts the provided clock */
u0 clock_start(clock * clock) {
  clock->start_time = platform_get_absolute_time();
  clock->elapsed = 0;
}
/* * * * * * * * * * * * * * */

/* stops the provided clock  */
u0 clock_stop(clock * clock) {
  clock->start_time = 0;
}
/* * * * * * * * * * * * * * */