#pragma once

#include "../defines.h"

#define LOG_WARN_ENABLED
#define LOG_INFO_ENABLED

/* only enable debug/trace 
   logging for debug builds  */
#ifndef M_RELEASE
#define LOG_DEBUG_ENABLED
#define LOG_TRACE_ENABLED
#endif
/* * * * * * * * * * * * * * */

typedef enum{
  LOG_LEVEL_FATAL = 0,
  LOG_LEVEL_ERROR,
  LOG_LEVEL_WARN,
  LOG_LEVEL_INFO,
  LOG_LEVEL_DEBUG,
  LOG_LEVEL_TRACE,
} log_level;

b8 initialize_logging(u0);
u0 shutdown_logging(u0);

M_API u0 log_output(log_level level, const char * const message, ...);

/* logs a fatal-level message  */ 
#ifndef M_FATAL
#define M_FATAL(message, ...) log_output(LOG_LEVEL_FATAL, message, ##__VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs an error-level message */ 
#ifndef M_ERROR
#define M_ERROR(message, ...) log_output(LOG_LEVEL_ERROR, message, ##__VA_ARGS__)
#endif
/* * * * * * * * * * * * * * * */

/* logs a warning-level message  */ 
#ifndef M_WARN
#ifdef LOG_WARN_ENABLED
#define M_WARN(message, ...) log_output(LOG_LEVEL_WARN, message, ##__VA_ARGS__)
#else
#define M_WARN(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * * */

/* logs an info-level message  */ 
#ifndef M_INFO
#ifdef LOG_INFO_ENABLED
#define M_INFO(message, ...) log_output(LOG_LEVEL_INFO, message, ##__VA_ARGS__)
#else
#define M_INFO(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */

/* logs a debug-level message  */ 
#ifndef M_DEBUG
#ifdef LOG_DEBUG_ENABLED
#define M_DEBUG(message, ...) log_output(LOG_LEVEL_DEBUG, message, ##__VA_ARGS__)
#else
#define M_DEBUG(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */

/* logs a trace-level message  */ 
#ifndef M_TRACE
#ifdef LOG_TRACE_ENABLED
#define M_TRACE(message, ...) log_output(LOG_LEVEL_TRACE, message, ##__VA_ARGS__)
#else
#define M_TRACE(message, ...)
#endif
#endif
/* * * * * * * * * * * * * * * */
