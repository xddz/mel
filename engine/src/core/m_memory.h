#pragma once

#include "../defines.h"

#include <stdio.h>

typedef enum memory_tag {
  MEMORY_TAG_UNKNOWN = 0, /* TODO: temporary */
  MEMORY_TAG_ARRAY,
  MEMORY_TAG_DARRAY,
  MEMORY_TAG_DICT,
  MEMORY_TAG_RING_QUEUE,
  MEMORY_TAG_BST,
  MEMORY_TAG_STRING,
  MEMORY_TAG_APPLICATION,
  MEMORY_TAG_JOB,
  MEMORY_TAG_TEXTURE,
  MEMORY_TAG_MATERIAL_INSTANCE,
  MEMORY_TAG_RENDERER,
  MEMORY_TAG_GAME,
  MEMORY_TAG_TRANSFORM,
  MEMORY_TAG_ENTITY,
  MEMORY_TAG_ENTITY_NODE,
  MEMORY_TAG_SCENE,

  MEMORY_TAG_MAX_TAGS
} memory_tag;

M_API u0 initialize_memory(u0);
M_API u0 shutdown_memory(u0);

M_API u0 * m_allocate(const u64 size,
                        const memory_tag tag);
M_API u0   m_free(u0 * block,
                    const u64 size,
                    const memory_tag tag);

M_API u0 * m_zero_memory(u0 * block,
                           const u64 size);
M_API u0 * m_copy_memory(u0 * dest,
                           const u0 * source,
                           const u64 size);
M_API u0 * m_set_memory(u0 * dest,
                          const s32 value,
                          const u64 size);

M_API u0 print_memory_usage(u0);
