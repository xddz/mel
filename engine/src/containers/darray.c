#include "./darray.h"
#include "../core/logger.h"
#include "../core/m_memory.h"

M_API u0 * _darray_create(const u64 length, const u64 stride){
  u64 header_size = DARRAY_FIELD_LENGTH * sizeof(u64);
  u64 array_size = length * stride;
  u64 * new_array = m_allocate(header_size + array_size, MEMORY_TAG_DARRAY);
  m_set_memory(new_array, 0, header_size + array_size);
  new_array[DARRAY_CAPACITY] = length;
  new_array[DARRAY_LENGTH] = 0;
  new_array[DARRAY_STRIDE] = stride;
  return (u0 *)(new_array + DARRAY_FIELD_LENGTH);
}

M_API u0 _darray_destroy(u0 * array){
  u64 * header = (u64 *)array - DARRAY_FIELD_LENGTH;
  u64 header_size = DARRAY_FIELD_LENGTH * sizeof(u64);
  u64 total_size = header_size + header[DARRAY_CAPACITY] * header[DARRAY_STRIDE];
  m_free(header, total_size, MEMORY_TAG_DARRAY);
}

M_API u64 _darray_field_get(u0 * array, const u64 field){
  u64 * header = (u64 *)array - DARRAY_FIELD_LENGTH;
  return header[field];
}

M_API u0 _darray_field_set(u0 * array, const u64 field, const u64 value){
  u64 * header = (u64 *)array - DARRAY_FIELD_LENGTH;
  header[field] = value;
}

M_API u0 * _darray_resize(u0 * array){
  u64 length = darray_length(array);
  u64 stride = darray_stride(array);
  u0 * tmp = _darray_create((DARRAY_RESIZE_FACTOR * darray_capacity(array)), stride);

  m_copy_memory(tmp, array, length * stride);
  _darray_field_set(tmp, DARRAY_LENGTH, length);
  _darray_destroy(array);
  return tmp;
}

M_API u0 * _darray_push(u0 * array, const u0 * value_ptr){
  u64 length = darray_length(array);
  u64 stride = darray_stride(array);
  if (length >= darray_capacity(array)){
    array = _darray_resize(array);
  }

  u64 addr = (u64)array;
  addr += (length * stride);
  m_copy_memory((u0 *)addr, value_ptr, stride);
  _darray_field_set(array, DARRAY_LENGTH, length + 1);
  return array;
}

M_API u0 _darray_pop(u0 * array, u0 * dest){
  u64 length = darray_length(array);
  u64 stride = darray_stride(array);
  u64 addr = (u64)array;
  addr += ((length - 1) * stride);
  m_copy_memory((u0 *)dest, (u0 *)addr, stride);
  _darray_field_set(array, DARRAY_LENGTH, length - 1);
}

M_API u0 * _darray_push_at(u0 * array, const u64 index, const u0 * value_ptr){
  u64 length = darray_length(array);
  u64 stride = darray_stride(array);
  if (index >= length){
    M_ERROR("Array index out of bounds! Length: %d, Index: %d", length, index);
    return array;
  }

  if (length >= darray_capacity(array)){
    array = _darray_resize(array);
  }

  u64 addr = (u64)array;

  if (index != (length - 1)){
    m_copy_memory((u0 *)(addr + ((index + 1) * stride)), 
                  (u0 *)(addr + (index * stride)),
                  stride * (length - index));
  }

  m_copy_memory((u0 *)(addr + (index * stride)), value_ptr, stride);
  _darray_field_set(array, DARRAY_LENGTH, length + 1);
  return array;
}

M_API u0 * _darray_pop_at(u0 * array, const u64 index, u0 * dest){
  u64 length = darray_length(array);
  u64 stride = darray_stride(array);
  if (index >= length){
    M_ERROR("Array index out of bounds! Length: %d, Index: %d", length, index);
    return array;
  }

  u64 addr = (u64)array;
  m_copy_memory(dest, (u0 *)(addr + (index * stride)), stride);

  if (index != (length - 1)){
    m_copy_memory((u0 *)(addr + (index * stride)), 
                  (u0 *)(addr + ((index + 1) * stride)),
                  stride * (length - index));
  }

  _darray_field_set(array, DARRAY_LENGTH, length - 1);
  return array;
}
