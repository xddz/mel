#pragma once

#include "../defines.h"

enum {
  DARRAY_CAPACITY,
  DARRAY_LENGTH,
  DARRAY_STRIDE,
  DARRAY_FIELD_LENGTH
};

M_API u0 * _darray_create(const u64 length, const u64 stride);
M_API u0  _darray_destroy(u0 * array);

M_API u64  _darray_field_get(u0 * array, const u64 field);
M_API u0  _darray_field_set(u0 * array, const u64 field, const u64 value);

M_API u0 * _darray_resize(u0 * array);

M_API u0 * _darray_push(u0 * array, const u0 * value_ptr);
M_API u0 _darray_pop(u0 * array, u0 * dest);

M_API u0 * _darray_push_at(u0 * array, const u64 index, const u0 * value_ptr);
M_API u0 * _darray_pop_at(u0 * array, const u64 index, u0 * dest);

#define DARRAY_DEFAULT_CAPACITY (1)
#define DARRAY_RESIZE_FACTOR    (2)

#define darray_create(type) _darray_create(DARRAY_DEFAULT_CAPACITY, sizeof(type))

#define darray_reserve(type, capacity) _darray_create(capacity, sizeof(type))

#define darray_destroy(array) _darray_destroy(array)

#define darray_push(array, value)                  \
  do {                                             \
    typeof(value) tmp = value;                     \
    array = _darray_push(array, &tmp);             \
  } while (0);

#define darray_pop(array, value_ptr) _darray_pop(array, value_ptr)

#define darray_push_at(array, index, value)        \
  do {                                             \
    typeof(value) tmp = value;                     \
    array = _darray_push_at(array, index, &tmp);   \
  } while (0);

#define darray_pop_at(array, index, dest) _darray_pop_at(array, index, dest)

#define darray_clear(array) _darray_field_set(array, DARRAY_LENGTH, 0)

#define darray_capacity(array) _darray_field_get(array, DARRAY_CAPACITY)

#define darray_length(array) _darray_field_get(array, DARRAY_LENGTH)

#define darray_stride(array) _darray_field_get(array, DARRAY_STRIDE)

#define darray_length_set(array, value) _darray_field_set(array, DARRAY_LENGTH, value)
