#include <core/logger.h>

#include "main.h"

#define trace_fn() M_DEBUG("%s called!", __FUNCTION__)

/* program initialization  */
b8 app_initialize(mel_t * ctx){
  trace_fn();
  return true;
}
/* * * * * * * * * * * * * */

/* program updating  */
b8 app_update(mel_t * ctx,
              const f32 delta_time){

  return true;
}
/* * * * * * * * * * */

/* program rendering */
b8 app_render(mel_t * ctx,
              const f32 delta_time){

  return true;
}
/* * * * * * * * * * */

/* window resizing */
void app_on_resize(mel_t * ctx,
                   const u16 width,
                   const u16 height){

  trace_fn();
}
/* * * * * * * * * */
