#pragma once

#include <defines.h>
#include <mel.h>

typedef struct{
  f32 delta_time;
} state;

b8   app_initialize(mel_t * ctx);
b8   app_update    (mel_t * ctx, f32 delta_time);
b8   app_render    (mel_t * ctx, f32 delta_time);
void app_on_resize (mel_t * ctx, u16 width, u16 height);
