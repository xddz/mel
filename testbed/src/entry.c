#include <entry.h>
#include "./main.h"

b8 mel(mel_t * ctx){

  ctx->app_config.start_pos_x  = 0;
  ctx->app_config.start_pos_y  = 0;
  ctx->app_config.start_width  = 1280;
  ctx->app_config.start_height = 720;
  ctx->app_config.name         = "mel testbed";

  ctx->initialize = app_initialize;
  ctx->update     = app_update;
  ctx->render     = app_render;
  ctx->on_resize  = app_on_resize;

  ctx->state = m_allocate(sizeof(state), MEMORY_TAG_GAME);

  return true;
}
